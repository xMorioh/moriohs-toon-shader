#region

using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using Object = UnityEngine.Object;

#endregion

// ---DISCLAIMER--- THIS CODE IS BASED OFF OF "SYNQARK"'s ARKTOON-SHADERS AND "XIEXE"'s UNITY-SHADERS. FOR MORE INFORMATION PLEASE REFER TO THE ORIGINAL BASE WRITER "https://github.com/synqark", "https://github.com/synqark/Arktoon-Shaders" or "https://github.com/Xiexe", "https://github.com/Xiexe/Xiexes-Unity-Shaders"
namespace Morioh.Moriohs_Toon_Shader.Editor
{
    public class MorisMaterialInspector : ShaderGUI
    {
        private readonly MaterialProperty _ShaderOptimizerEnabled = null;
    
        //UV Switches
        private readonly MaterialProperty _OutlineMaskUVSwitch = null;
        private readonly MaterialProperty _DitherMaskUVSwitch = null;
        private readonly MaterialProperty _EmissionScrollMaskUVSwitch = null;
        private readonly MaterialProperty _EmissionScrollMaskTexChannelSwitch = null;
        //private readonly MaterialProperty _EmissionUVSwitch = null;
        private readonly MaterialProperty _EmissionMapUVSwitch = null;
        private readonly MaterialProperty _MainTexUVSwitch = null;
        //private readonly MaterialProperty _DissolvePatternUVSwitch = null;
        private readonly MaterialProperty _EmissionscrollUVSwitch = null;
        private readonly MaterialProperty _NoiseTextureUVSwitch = null;
        private readonly MaterialProperty _ReflectionMaskUVSwitch = null;
        private readonly MaterialProperty _ReflectionMaskTexChannelSwitch = null;
        private readonly MaterialProperty _MatcapMaskUVSwitch = null;
        //private readonly MaterialProperty _NormalMapUVSwitch = null;
        //private readonly MaterialProperty _SecondaryNormalMaskUVSwitch = null;
        //private readonly MaterialProperty _SecondaryNormalUVSwitch = null;
        private readonly MaterialProperty _RimMaskUVSwitch = null;
        private readonly MaterialProperty _RimMaskTexChannelSwitch = null;
        private readonly MaterialProperty _SSSThicknessMapUVSwitch = null;
        private readonly MaterialProperty _AnisoDirUVSwitch = null;
        private readonly MaterialProperty _SpecularMapUVSwitch = null;
        private readonly MaterialProperty _MetallicGlossMapUVSwitch = null;
        private readonly MaterialProperty _FlipbookUVSwitch = null;
        private readonly MaterialProperty _HueMaskUVSwitch = null;

        //Render Options
        private readonly MaterialProperty _Mode = null;
        private readonly MaterialProperty _ModeCustom = null;
        private readonly MaterialProperty _CullMode = null;
        //private readonly MaterialProperty _SourceBlendRGB = null;
        private readonly MaterialProperty _SrcBlend = null;
        //private readonly MaterialProperty _DestinationBlendRGB = null;
        private readonly MaterialProperty _DstBlend = null;
        private readonly MaterialProperty _BlendOpRGB = null;
        private readonly MaterialProperty _SourceBlendAlpha = null;
        private readonly MaterialProperty _DestinationBlendAlpha = null;
        private readonly MaterialProperty _BlendOpAlpha = null;
        //private readonly MaterialProperty _MaskClipValue = null; //Not used anymore
        private readonly MaterialProperty _AlphatoCoverage = null;
        private readonly MaterialProperty _ColorMask = null;
        private readonly MaterialProperty _StencilBufferReference = null;
        private readonly MaterialProperty _StencilBufferReadMask = null;
        private readonly MaterialProperty _StencilBufferWriteMask = null;
        private readonly MaterialProperty _StencilBufferComparison = null;
        private readonly MaterialProperty _StencilBufferPassFront = null;
        private readonly MaterialProperty _StencilBufferFailFront = null;
        private readonly MaterialProperty _StencilBufferZFailFront = null;
        //private readonly MaterialProperty _ZWriteMode = null;
        private readonly MaterialProperty _ZWrite = null;
        private readonly MaterialProperty _ZTestMode = null;
        private readonly MaterialProperty _DepthOffsetFactor = null;
        private readonly MaterialProperty _DepthOffsetUnits = null;
        private readonly MaterialProperty _ForceNoShadowCasting = null;
        private readonly MaterialProperty _LightModes = null;

        // Keyword Properties
        //private readonly MaterialProperty _COLORADDSUBDIFF = null;
        private readonly MaterialProperty _GlossyReflections = null;
        private readonly MaterialProperty _COLORCOLOR = null;

        // Hue Shift
        private readonly MaterialProperty _HueMaskinverter = null;
        private readonly MaterialProperty _HueShiftblend = null;
        private readonly MaterialProperty _HueShiftSpeed = null;
        private readonly MaterialProperty _HueShiftRandomizer = null;
        private readonly MaterialProperty _HueMask = null;
        private readonly MaterialProperty _ToggleHueTexforSpeed = null;

        //Dither
        private readonly MaterialProperty _StartDitheringFade = null;
        private readonly MaterialProperty _EndDitheringFade = null;
        private readonly MaterialProperty _DitherTextureToggle = null;
        private readonly MaterialProperty _DitherTexture = null;
        private readonly MaterialProperty _DitherTextureTiling = null;
        private readonly MaterialProperty _DitherAlphaToggle = null;
        private readonly MaterialProperty _DitherMask = null;

        //Dissolve
        //private readonly MaterialProperty _MaterializeVertexColor = null;
        //private readonly MaterialProperty _DissolvePattern = null;
        //private readonly MaterialProperty _DissolveMask = null;
        //private readonly MaterialProperty _MaterializeTex = null;
        //private readonly MaterialProperty _DissolveDensity = null;
        //private readonly MaterialProperty _DissolveModifier = null;
        //private readonly MaterialProperty _MaterializeR = null;
        //private readonly MaterialProperty _MaterializeG = null;
        //private readonly MaterialProperty _MaterializeB = null;
        //private readonly MaterialProperty _MaterializeA = null;
        //private readonly MaterialProperty _ToggleDissolveDirInv = null;
        //private readonly MaterialProperty _ToggleMaterializeDirInv = null;
        //private readonly MaterialProperty _ToggleDissolveEmission = null;
        //private readonly MaterialProperty _ToggleDissolveVertexOffset = null;
        //private readonly MaterialProperty _DissolveVertexMultiplier = null;
        //private readonly MaterialProperty _EmissiveDissolveColor = null;
        //private readonly MaterialProperty _MaterializeColorLayerR = null;
        //private readonly MaterialProperty _MaterializeColorLayerG = null;
        //private readonly MaterialProperty _MaterializeColorLayerB = null;
        //private readonly MaterialProperty _MaterializeColorLayerA = null;
        //private readonly MaterialProperty _MaterializeLayerModeR = null;
        //private readonly MaterialProperty _MaterializeLayerModeG = null;
        //private readonly MaterialProperty _MaterializeLayerModeB = null;
        //private readonly MaterialProperty _MaterializeLayerModeA = null;
        //private readonly MaterialProperty _DissolveRemapMin = null;
        //private readonly MaterialProperty _DissolveRemapMax = null;
    
        //Inventory System
        private readonly MaterialProperty _UseInventory = null;
        private readonly MaterialProperty _InventoryComplexity = null;
        private readonly MaterialProperty _InventoryComplexityTooltip = null;
        private readonly MaterialProperty _InventoryStride = null;
        private readonly MaterialProperty _InventoryItem01Animated = null;
        private readonly MaterialProperty _InventoryItem02Animated = null;
        private readonly MaterialProperty _InventoryItem03Animated = null;
        private readonly MaterialProperty _InventoryItem04Animated = null;
        private readonly MaterialProperty _InventoryItem05Animated = null;
        private readonly MaterialProperty _InventoryItem06Animated = null;
        private readonly MaterialProperty _InventoryItem07Animated = null;
        private readonly MaterialProperty _InventoryItem08Animated = null;
        private readonly MaterialProperty _InventoryItem09Animated = null;
        private readonly MaterialProperty _InventoryItem10Animated = null;
        private readonly MaterialProperty _InventoryItem11Animated = null;
        private readonly MaterialProperty _InventoryItem12Animated = null;
        private readonly MaterialProperty _InventoryItem13Animated = null;
        private readonly MaterialProperty _InventoryItem14Animated = null;
        private readonly MaterialProperty _InventoryItem15Animated = null;
        private readonly MaterialProperty _InventoryItem16Animated = null;
        private readonly MaterialProperty _EmissiveDissolveColor = null;
        private readonly MaterialProperty _InventoryEmissionThickness = null;
        private readonly MaterialProperty _InventoryUVSwitch = null;
        private readonly MaterialProperty _DissolvePattern = null;
        private readonly MaterialProperty _InventoryRenameProperties = null;
        //
        private readonly MaterialProperty _Saturation = null;
        private readonly MaterialProperty _Color = null;
        private readonly MaterialProperty _BC7compressionFix = null;
        private readonly MaterialProperty _MainTex = null;
        private readonly MaterialProperty _OcclusionMap = null;
        private readonly MaterialProperty _OcclusionMapUVSwitch = null;
        //private readonly MaterialProperty _Occlusion = null;
        private readonly MaterialProperty _OcclusionStrength = null;
        private readonly MaterialProperty _Steps = null;
        private readonly MaterialProperty _NdLHalfingControl = null;
        private readonly MaterialProperty _ColoringPointLights = null;
        private readonly MaterialProperty _ColoringDirectEnvLights = null;
        private readonly MaterialProperty _ToggleMonochromePixelLight = null;
        private readonly MaterialProperty _AmbientBoost = null;
        private readonly MaterialProperty _ToggleMonochromeEnv = null;
        private readonly MaterialProperty _DistanceFade = null;
        private readonly MaterialProperty _RimColor = null;
        private readonly MaterialProperty _ShadowMask = null;
        private readonly MaterialProperty _ShadowMaskTexChannelSwitch = null;
        private readonly MaterialProperty _ShadowMaskStrength = null;
        private readonly MaterialProperty _ShadowColorMap = null;
        private readonly MaterialProperty _ShadowColorMapStrength = null;
        private readonly MaterialProperty _ToonRamp = null;
        private readonly MaterialProperty _RampColor = null;
        private readonly MaterialProperty _RampOffset = null;
        private readonly MaterialProperty _RimMask = null;
        private readonly MaterialProperty _RimTint = null;
        private readonly MaterialProperty _RimOpacity = null;
        private readonly MaterialProperty _RimLightReflInheritAniso = null;
        private readonly MaterialProperty _RimPower = null;
        private readonly MaterialProperty _RimOffset = null;
        private readonly MaterialProperty _RimDirectionToggle = null;
        private readonly MaterialProperty _RimFresnelBias = null;
        private readonly MaterialProperty _RimFresnelScale = null;
        private readonly MaterialProperty _RimFresnelPower = null;
        private readonly MaterialProperty _RimSpecLightsmoothness = null;
        private readonly MaterialProperty _RimFaceCulling = null;
        private readonly MaterialProperty _DirectShadowIntensity = null;
        private readonly MaterialProperty _PointSpotShadowIntensity = null;
        private readonly MaterialProperty _IndirectShadowIntensity = null;
        private readonly MaterialProperty _SelfCastShadows = null; //everytime im working on stuff like my Lighting system at 3AM stuff like this happens.. Its for the Shadow caster.
        private readonly MaterialProperty _MaxLightDirect = null;
        private readonly MaterialProperty _LightLimiter = null;
        private readonly MaterialProperty _RimSwitch = null;
        private readonly MaterialProperty _RimHueSpeed = null;
        private readonly MaterialProperty _ShadowRimOpacity = null;
        private readonly MaterialProperty _ShadowRimRange = null;
        private readonly MaterialProperty _ShadowRimSharpness = null;
        private readonly MaterialProperty _EmissiveRimColor = null;
        private readonly MaterialProperty _HighlightOffset = null;
        private readonly MaterialProperty _HighlightSmoothness = null;
        private readonly MaterialProperty _EnableGSAA = null;
        private readonly MaterialProperty _GSAAVariance = null;
        private readonly MaterialProperty _GSAAThreshold = null;
        private readonly MaterialProperty _SpecularTint = null;
        private readonly MaterialProperty _SpecularMap = null;
        private readonly MaterialProperty _AnisoDir = null;
        private readonly MaterialProperty _AnisoScale = null;
        private readonly MaterialProperty _AnisoSharpening = null;
        private readonly MaterialProperty _BlinntoAniso = null;
        private readonly MaterialProperty _Anisotropy = null;
        //private readonly MaterialProperty _AnisoF0Reflectance = null; //Not used anymore
        private readonly MaterialProperty _AnisoFlickerFix = null;
        private readonly MaterialProperty _AnisoFlickerFixOffset = null;
        private readonly MaterialProperty _SpecularColor = null;
        private readonly MaterialProperty _SpecularSetting = null;
        private readonly MaterialProperty _SpecShadowMaskVar = null;
        private readonly MaterialProperty _SpecShadowMaskPower = null;
        private readonly MaterialProperty _EmissionColor = null;
        //private readonly MaterialProperty _Emission = null;
        private readonly MaterialProperty _EmissionMap = null;
        private readonly MaterialProperty _EmissionLightscale = null;
        private readonly MaterialProperty _Metallic = null;
        private readonly MaterialProperty _CubemapIntensity = null;
        private readonly MaterialProperty _Cubemap = null;
        //private readonly MaterialProperty _Cubemapsmoothness = null;
        private readonly MaterialProperty _Glossiness = null;
        private readonly MaterialProperty _MetallicGlossMap = null;
        //private readonly MaterialProperty _CubemapSpecularToggle = null;
        private readonly MaterialProperty _IgnoreNormalsCubemap = null;
        private readonly MaterialProperty _ESVoronoiScale = null;
        private readonly MaterialProperty _ESVoronoiSpeed = null;
        private readonly MaterialProperty _EmissionscrollTint = null;
        private readonly MaterialProperty _EmissionscrollColor = null;
        private readonly MaterialProperty _Emissionscroll = null;
        private readonly MaterialProperty _Emiossionscrollspeed = null;
        private readonly MaterialProperty _ESScrollOffset = null;
        private readonly MaterialProperty _NoiseTexture = null;
        private readonly MaterialProperty _NoiseSpeed = null;
        private readonly MaterialProperty _NoiseVectorXY = null;
        private readonly MaterialProperty _ReflectionMask = null;
        private readonly MaterialProperty _ToggleDisneyDiffuse = null;
        private readonly MaterialProperty _WorkflowSwitch = null;
        private readonly MaterialProperty _BRDFReflInheritAniso = null;
        private readonly MaterialProperty _ESRenderMethod = null;
        private readonly MaterialProperty _EmissionScrollMask = null;
        private readonly MaterialProperty _VectorXY = null;
        private readonly MaterialProperty _IgnoreNormalsESv2 = null;

        //Audio
        private readonly MaterialProperty _AudioBandIntensity = null;
        private readonly MaterialProperty _AudioHueSpeed = null;
        private readonly MaterialProperty _WaveformCoordinates = null;
        private readonly MaterialProperty _WaveformUVShift = null;
        private readonly MaterialProperty _WaveformRotation = null;
        private readonly MaterialProperty _AudioLinkSwitch = null;
        private readonly MaterialProperty _AudioLinkColor = null;
        private readonly MaterialProperty _AudioLinkTooltip = null;
        private readonly MaterialProperty _AudioLinkBandHistory = null;
        private readonly MaterialProperty _AudioLinkWaveformMirrorToggle = null;
        private readonly MaterialProperty _WaveformThickness = null;

        //end Audio
        private readonly MaterialProperty _ESSpeed = null;
        private readonly MaterialProperty _ESCoordinates = null;
        private readonly MaterialProperty _ESSize = null;
        private readonly MaterialProperty _ESSharpness = null;
        private readonly MaterialProperty _ESLevelOffset = null;
        private readonly MaterialProperty _EmissionTint = null;
        //private readonly MaterialProperty _NormalMap = null;
        //private readonly MaterialProperty _NormalScale = null;
        private readonly MaterialProperty _BumpMap = null;
        private readonly MaterialProperty _BumpScale = null;
        private readonly MaterialProperty _BumpMapUVSwitch = null;
        //private readonly MaterialProperty _SecondaryNormal = null;
        private readonly MaterialProperty _DetailNormalMap = null;
        private readonly MaterialProperty _DetailNormalMapScale = null;
        private readonly MaterialProperty _DetailNormalMapUVSwitch = null;
        //private readonly MaterialProperty _SecondaryNormalScale = null;
        //private readonly MaterialProperty _SecondaryNormalMask = null;
        private readonly MaterialProperty _DetailMaskUVSwitch = null;
        private readonly MaterialProperty _DetailMaskTexChannelSwitch = null;
        private readonly MaterialProperty _DetailMask = null;
        private readonly MaterialProperty _SubsurfaceDistortionModifier = null;
        private readonly MaterialProperty _SSSPower = null;
        private readonly MaterialProperty _SSSSetting = null;
        private readonly MaterialProperty _SSSScale = null;
        private readonly MaterialProperty _SSSColor = null;
        private readonly MaterialProperty _SSSTint = null;
        private readonly MaterialProperty _SSSThicknessMap = null;
        private readonly MaterialProperty _SSSThicknessinv = null;
        private readonly MaterialProperty _SSSMapMode = null;
        private readonly MaterialProperty _Flipbook = null;
        private readonly MaterialProperty _Columns = null;
        private readonly MaterialProperty _Rows = null;
        private readonly MaterialProperty _Speed = null;
        private readonly MaterialProperty _MaxFrames = null;
        private readonly MaterialProperty _FlipbookColor = null;
        private readonly MaterialProperty _RotateFlipbook = null;
        private readonly MaterialProperty _FlipbookAudioLinkToggle = null;
        private readonly MaterialProperty _FlipbookUVShift = null;
        private readonly MaterialProperty _FlipbookToggle = null;
        private readonly MaterialProperty _FlipbookTint = null;
        private readonly MaterialProperty _FlipbookMode = null;
        private readonly MaterialProperty _SSSToggle = null;
        private readonly MaterialProperty _SpecularToggle = null;
        private readonly MaterialProperty _SpecularHighlights = null;
        private readonly MaterialProperty _RimToggle = null;
        private readonly MaterialProperty _RimSpecToggle = null;
        private readonly MaterialProperty _EmissionScrollToggle = null;
        private readonly MaterialProperty _ToggleSteps = null;
        //private readonly MaterialProperty _Cutout = null;
        private readonly MaterialProperty _Cutoff = null;
        private readonly MaterialProperty _OutlineMode = null;
        private readonly MaterialProperty _OutlineHueSpeed = null;
        private readonly MaterialProperty _OutlineColor = null;
        private readonly MaterialProperty _OutlineWidth = null;
        private readonly MaterialProperty _OutlineDepthFadeDistance = null;
        private readonly MaterialProperty _OutlineDistancethickening = null;
        private readonly MaterialProperty _OutlineTint = null;
        private readonly MaterialProperty _OutlineMask = null;

        //Inspector defined settings
        private readonly MaterialProperty _AdvancedExperimentalToggle = null;
        private readonly MaterialProperty _IgnoreProjector = null;

        //MATCAP
        private readonly MaterialProperty _MatcapR1 = null;
        private readonly MaterialProperty _MatcapG2 = null;
        private readonly MaterialProperty _MatcapB3 = null;
        private readonly MaterialProperty _MatcapA4 = null;
        private readonly MaterialProperty _MatcapR1Toggle = null;
        private readonly MaterialProperty _MatcapG2Toggle = null;
        private readonly MaterialProperty _MatcapB3Toggle = null;
        private readonly MaterialProperty _MatcapA4Toggle = null;
        private readonly MaterialProperty _IgnoreNormalsMatcap = null;
        private readonly MaterialProperty _MatcapR1Mode = null;
        private readonly MaterialProperty _MatcapG2Mode = null;
        private readonly MaterialProperty _MatcapB3Mode = null;
        private readonly MaterialProperty _MatcapA4Mode = null;
        private readonly MaterialProperty _MatcapR1Blending = null;
        private readonly MaterialProperty _MatcapG2Blending = null;
        private readonly MaterialProperty _MatcapB3Blending = null;
        private readonly MaterialProperty _MatcapA4Blending = null;
        private readonly MaterialProperty _MatcapR1Intensity = null;
        private readonly MaterialProperty _MatcapG2Intensity = null;
        private readonly MaterialProperty _MatcapB3Intensity = null;
        private readonly MaterialProperty _MatcapA4Intensity = null;
        private readonly MaterialProperty _MatcapR1smoothness = null;
        private readonly MaterialProperty _MatcapG2smoothness = null;
        private readonly MaterialProperty _MatcapB3smoothness = null;
        private readonly MaterialProperty _MatcapA4smoothness = null;
        private readonly MaterialProperty _MatcapR1Color = null;
        private readonly MaterialProperty _MatcapG2Color = null;
        private readonly MaterialProperty _MatcapB3Color = null;
        private readonly MaterialProperty _MatcapA4Color = null;
        private readonly MaterialProperty _MatcapR1Tint = null;
        private readonly MaterialProperty _MatcapG2Tint = null;
        private readonly MaterialProperty _MatcapB3Tint = null;
        private readonly MaterialProperty _MatcapA4Tint = null;
        private readonly MaterialProperty _MatcapR1MaskInvert = null;
        private readonly MaterialProperty _MatcapG2MaskInvert = null;
        private readonly MaterialProperty _MatcapB3MaskInvert = null;
        private readonly MaterialProperty _MatcapA4MaskInvert = null;
        private readonly MaterialProperty _MatcapMask = null;
        private readonly MaterialProperty _MatcapViewDir = null;
        //LTCGI
        private readonly MaterialProperty _LTCGI = null;
        private readonly MaterialProperty _ToggleLTCGIDiffuse = null;
        private readonly MaterialProperty _ToggleLTCGISpecular = null;
        private readonly MaterialProperty _LTCGInotInstalledTooltip = null;
        //Decals
        private readonly MaterialProperty _Decal1 = null;
        private readonly MaterialProperty _Decal1Mode = null;
        private readonly MaterialProperty _RotateDecal1 = null;
        private readonly MaterialProperty _Decal1AudioLinkToggle = null;
        private readonly MaterialProperty _Decal1WrapMode = null;
        private readonly MaterialProperty _Decal1UVSwitch = null;
        private readonly MaterialProperty _Decal1Tint = null;
        private readonly MaterialProperty _Decal1Color = null;
        private readonly MaterialProperty _Decal2 = null;
        private readonly MaterialProperty _Decal2Mode = null;
        private readonly MaterialProperty _RotateDecal2 = null;
        private readonly MaterialProperty _Decal2AudioLinkToggle = null;
        private readonly MaterialProperty _Decal2WrapMode = null;
        private readonly MaterialProperty _Decal2UVSwitch = null;
        private readonly MaterialProperty _Decal2Tint = null;
        private readonly MaterialProperty _Decal2Color = null;
        private readonly MaterialProperty _Decal3 = null;
        private readonly MaterialProperty _Decal3Mode = null;
        private readonly MaterialProperty _RotateDecal3 = null;
        private readonly MaterialProperty _Decal3AudioLinkToggle = null;
        private readonly MaterialProperty _Decal3WrapMode = null;
        private readonly MaterialProperty _Decal3UVSwitch = null;
        private readonly MaterialProperty _Decal3Tint = null;
        private readonly MaterialProperty _Decal3Color = null;
        private readonly MaterialProperty _Decal4 = null;
        private readonly MaterialProperty _Decal4Mode = null;
        private readonly MaterialProperty _RotateDecal4 = null;
        private readonly MaterialProperty _Decal4AudioLinkToggle = null;
        private readonly MaterialProperty _Decal4WrapMode = null;
        private readonly MaterialProperty _Decal4UVSwitch = null;
        private readonly MaterialProperty _Decal4Tint = null;
        private readonly MaterialProperty _Decal4Color = null;
        private readonly MaterialProperty _DecalUVShift = null;
        private readonly MaterialProperty _DecalToggle = null;
        private readonly MaterialProperty _DecalMask = null;
        private readonly MaterialProperty _DecalMaskUVSwitch = null;
        
        
        
    
        // private readonly MaterialProperty _LockTooltip = null; //for Kaj Optimizer Info Field in Inspector
        private static bool showMainSettings = true;
        private static bool showShadowSettings = true;
        private static bool showDither;
        //private static bool showDissolve;
        private static bool showInventory;
        private static bool showShadowEmissiveRim;
        private static bool showSpecular;
        private static bool showRimlight;
        private static bool showReflections;
        private static bool showMatcapR1;
        private static bool showMatcapG2;
        private static bool showMatcapB3;
        private static bool showMatcapA4;
        private static bool showEmission;
        private static bool showEmissionScroll;
        private static bool showNormals;
        private static bool showLTCGI;
        private static bool showFlipbook;
        private static bool showSSS;
        private static bool showOutlines;
        private static bool showMiscellaneous;
        private static bool showMainTexHueShift;
        private static bool showRenderingOptions;
        
    
        private readonly BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
    
        private readonly string NewShadername = "Moriohs Shaders/Moris Toon Shader/Toon"; //new shader name global define, needs full name here
        private bool isNewShader;
        private static readonly int Mode = Shader.PropertyToID("_Mode");                                     //used several times so we instance it
        private static readonly int ShaderOptimizerEnabled = Shader.PropertyToID("_ShaderOptimizerEnabled"); //used several times so we instance it
        private static readonly int SrcBlend = Shader.PropertyToID("_SrcBlend");                             //used several times so we instance it
        private static readonly int DstBlend = Shader.PropertyToID("_DstBlend");                             //used several times so we instance it
        private static readonly int ZWrite = Shader.PropertyToID("_ZWrite");                                 //used several times so we instance it
        private static readonly int SourceBlendAlpha = Shader.PropertyToID("_SourceBlendAlpha");
        private static readonly int DestinationBlendAlpha = Shader.PropertyToID("_DestinationBlendAlpha");
        private static readonly int BlendOpAlpha = Shader.PropertyToID("_BlendOpAlpha");
    

        private enum BlendMode
        {
            Opaque,
            Cutout,
            Fade,       // Old school alpha-blending mode, fresnel does not affect amount of transparency
            Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
        }


        //Setup correct Blendmode and Button for Switching to new Shader
        private void SwitchShaderButton(MaterialEditor materialEditor, Material material)
        {
            if (materialEditor.targets.Length != 1) return; //cannot switch more than one selected Material, icky but only needs doing once for each old material
            bool currentlyOldShaderOpaque = material.shader.name.Contains("Opaque");
            bool currentlyOldShaderCutout = material.shader.name.Contains("Cutout");
            bool currentlyOldShaderTransparent = material.shader.name.Contains("Transparent");
            EditorGUI.BeginChangeCheck();
            GUILayout.Button("The Shader you are using is depracted, Switch to new Shader");
            if (EditorGUI.EndChangeCheck())
            {
                BlendMode blendMode; //Setup Blendmode
                if (_ShaderOptimizerEnabled.floatValue == 1) material.SetFloat(ShaderOptimizerEnabled,0);
                if (currentlyOldShaderOpaque)
                {
                    Shader finalShader = Shader.Find(NewShadername);
                    // If we can't find it, pass.
                    if (finalShader != null)
                    {
                        material.shader = finalShader;
                    }
                    blendMode = BlendMode.Opaque;
                    material.SetFloat(Mode, (float)blendMode);
                }
            
                else if (currentlyOldShaderCutout)
                {
                    Shader finalShader = Shader.Find(NewShadername);
                    // If we can't find it, pass.
                    if (finalShader != null)
                    {
                        material.shader = finalShader;
                    }
                    blendMode = BlendMode.Cutout;
                    material.SetFloat(Mode, (float)blendMode);
                }
            
                else if (currentlyOldShaderTransparent)
                {
                    Shader finalShader = Shader.Find(NewShadername);
                    // If we can't find it, pass.
                    if (finalShader != null)
                    {
                        material.shader = finalShader;
                    }
                    blendMode = BlendMode.Transparent;
                    material.SetFloat(Mode, (float)blendMode);
                }
            }
        }
    
        //Setup correct Blendmode for manually Switching Shaders
        public override void AssignNewShaderToMaterial(Material material, Shader oldShader, Shader newShader)
        {
            //We need to make sure that the Shader Optimizer is always toggled off when switching to another Shader
            //Do this only for Shaders that have _ShaderOptimizerEnabled as property defined to outrun instance exceptions
            //For that we get the value of the Property from the material and check if it's active or not, if not we do nothing
            bool hasShaderOptimizer = material.HasProperty(ShaderOptimizerEnabled);
            if (hasShaderOptimizer) 
            {
                float ShaderOptimizerValue = material.GetFloat(ShaderOptimizerEnabled);
                if (ShaderOptimizerValue == 1)
                {
                    material.SetFloat(ShaderOptimizerEnabled,0);
                }
            }
        
            base.AssignNewShaderToMaterial(material, oldShader, newShader); //Defining new and old Shaders so we can work with them

            //Choose which Blendmode to assign based on the variables
            //Can't use "isCutout" here for example since this value won't have the correct value assigned at this stage
            //We could define it here but then the old shader assignment below might bug out
            if (!material.HasProperty("_Mode")) //Make sure that switching from shaders that contained _Mode to inherit their BlendMode
            {
                BlendMode blendMode = BlendMode.Opaque;
                if (oldShader.name.Contains("Cutout"))
                {
                    blendMode = BlendMode.Cutout;
                }

                if (oldShader.name.Contains("Transparent"))
                {
                    blendMode = BlendMode.Transparent;
                }

                material.SetFloat(Mode, (float)blendMode);
            }
        }
    
    
        //Setup Blend Mode like Standard Shader
        private static void SetupMaterialWithBlendMode(Material material, BlendMode blendMode)
        {
            if (material.GetInt("_ModeCustom") != 1)
            {
                switch (blendMode)
                {
                    case BlendMode.Opaque:
                        material.SetOverrideTag("RenderType", "");
                        material.SetInt(SrcBlend, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(DstBlend, (int) UnityEngine.Rendering.BlendMode.Zero);
                        material.SetInt(ZWrite, 1);
                        material.renderQueue = -1;
                        break;
                    case BlendMode.Cutout:
                        material.SetOverrideTag("RenderType", "TransparentCutout");
                        material.SetInt(SrcBlend, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(DstBlend, (int) UnityEngine.Rendering.BlendMode.Zero);
                        material.SetInt(ZWrite, 1);
                        material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.AlphaTest;
                        break;
                    case BlendMode.Fade:
                        material.SetOverrideTag("RenderType", "TransparentCutout");
                        material.SetInt(SrcBlend, (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
                        material.SetInt(DstBlend, (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        //Render Texture Alpha fix by d4rkpl4y3r source: "https://orels1.notion.site/Better-Alpha-blending-in-Render-Targets-d3bb80c242824245ac0ec7921c77e8a8"
                        material.SetInt(SourceBlendAlpha, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(DestinationBlendAlpha, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(BlendOpAlpha, (int) UnityEngine.Rendering.BlendOp.Max);
                        //
                        material.SetInt(ZWrite, 1);
                        material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.AlphaTest;
                        break;
                    case BlendMode.Transparent:
                        material.SetOverrideTag("RenderType", "TransparentCutout");
                        material.SetInt(SrcBlend, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(DstBlend, (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        material.SetInt(SourceBlendAlpha, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(DestinationBlendAlpha, (int) UnityEngine.Rendering.BlendMode.One);
                        material.SetInt(BlendOpAlpha, (int) UnityEngine.Rendering.BlendOp.Max);
                        material.SetInt(ZWrite, 1);
                        material.renderQueue = 2500;
                        break;
                }
            }
        }
        

        public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
        {
            var material = materialEditor.target as Material;
            var shader = material.shader;
            isNewShader = shader.name.Contains(NewShadername);
            bool isOutlined = shader.name.Contains("Outlined");

            int BlendModeCheck = material.GetInt(Mode);
            bool BlendModeCustomCheck = material.GetInt("_ModeCustom") == 1;
            bool isOpaque = BlendModeCheck == 0 && !BlendModeCustomCheck;
            bool isCutout = BlendModeCheck == 1 || BlendModeCustomCheck;
            bool isTransparent = BlendModeCheck == 2 || BlendModeCheck == 3 || BlendModeCustomCheck;

            SetupMaterialWithBlendMode(material, (BlendMode)BlendModeCheck);

            // Catch-block
            foreach (var property in GetType().GetFields(bindingFlags))
                if (property.FieldType == typeof(MaterialProperty))
                    try
                    {
                        property.SetValue(this, FindProperty(property.Name, props));
                    }
                    catch
                    {
                        // ignored
                    }
        
            //Setup Standard Shader properies
            //Unitys Standard Shader branches over two values if GlossMap is used, we only use one
            material.SetFloat("_GlossMapScale", material.GetFloat("_Glossiness"));

            //SHADER INSPECTOR START
            Moristyles.ShurikenHeaderCentered(Moristyles.ver);
            ShaderOptimizerButton(_ShaderOptimizerEnabled, materialEditor);
            if (!isNewShader && !isOutlined)SwitchShaderButton(materialEditor, material);
            EditorGUI.BeginChangeCheck();
            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
            {
                //materialEditor.ShaderProperty(_LockTooltip, new GUIContent("ALWAYS LOCK YOUR MATERIALS BEFORE ASSET BUILD", ""));
                materialEditor.ShaderProperty(_AdvancedExperimentalToggle, new GUIContent("Settings Level", "Hides a certain amount of settings for the User which are mostly just Texture Tiling options and things you might not change on your daily basis to have the GUI cleaned up a little."));
                materialEditor.ShaderProperty(_COLORCOLOR,new GUIContent("Toggle Advanced", "Toggles between a Basic and Advanced version of the Shader."));
                Moristyles.PartingLine();
                materialEditor.ShaderProperty(_CullMode, new GUIContent("Culling Mode", "Changes the culling mode. 'Off' will result in a two sided material, while 'Front' and 'Back' will cull those sides respectively"));
                if (!BlendModeCustomCheck)
                {
                    ShaderPropertyOptimizer(_Mode, null, null, "Rendering Mode", "Like in Standard Shader you control what the Shader will be used for");
                }
                ShaderPropertyOptimizer(_ModeCustom, null, null, "Custom Rendering Mode", "unlocks Blend Options, Renderqueue and some more things in the Rendering Options section\nBehaves like Rendering Mode (Transparent) but you get to choose what to set, keep in mind that this means that you are working in a Alpha Premultiply Setup\nIf you dont know what that means but you still want to change those locked settings then set the Rendering Mode to Transparent before you toggle this property");
            }

            showMainSettings = Moristyles.ShurikenFoldout("Main Settings", showMainSettings, 0);
            if (showMainSettings)
            {
                //Disable animated state toggle for _Color, _Color always needs to be animated
                materialEditor.TexturePropertySingleLine(new GUIContent("Main Texture", "This is where the main texture for the material goes. By changing the (Main Color), the (Main Tex) will change its Color depending on the settings you make"), _MainTex, _Color, _MainTexUVSwitch);
                material.SetFloat("_ColorAnimated", 1); //needs to be animated at all times
                material.SetFloat("_MainTexUVSwitchAnimated", 1); //Is animated because it does not have a toggle in the UI
                ShaderPropertyOptimizer(_Saturation, null, null, "MainTex Saturation", "Controls saturation of the final output from the shader.");
                if (isCutout || isTransparent)
                    ShaderPropertyOptimizer(_Cutoff, null, null, "Cutoff", "The Alpha Cutoff Amount, default value is 0 for Outline and Transparent Shader");
                else
                    material.SetFloat("_Cutoff", 0f);

                if (!isOpaque && _MainTex.textureValue != null && _MainTex.textureValue.graphicsFormat == GraphicsFormat.RGBA_BC7_SRGB && Path.GetExtension(AssetDatabase.GetAssetPath(_MainTex.textureValue.GetInstanceID())) != ".dds")
                    ShaderPropertyOptimizer(_BC7compressionFix, null, null, "BC7 Compression Fix", "You are using a BC7 compressed texture, this shader tries to to mitigate a bug in alpha values if a given texture is BC7 Compressed\nNOTE that this raises the last alpha values by +1 out of 255\nSo if you set an area to be 254 specifically then it will end up becoming 255 (no other values will be affected)\nIf you use an alpha value of 254 then you should lower these values by 1 manually in the texture and then activate this function or leave this function off instead\nMake sure to also set (Edit -> Project Settings -> Editor -> BC7 Compressor) to (ISPC legacy) this will greatly reduce the artifacts\nYou could also use the Nvidia Texture Tools Exporter instead of all this and have your texture compressed without issues in the first place");
                else
                    material.SetInt("_BC7compressionFix", 0);
            }
            if (EditorGUI.EndChangeCheck() && (_Mode.floatValue == 2 || _Mode.floatValue == 3 || _ModeCustom.floatValue == 1 || (_Mode.floatValue == 1 && _Cutoff.floatValue > 0)))
                material.SetInt("_IgnoreProjector", 1); //Force no Projector receiving when in Transparent mode

            showShadowSettings = Moristyles.ShurikenFoldout("Shadow and Light", showShadowSettings, 0);
            if (showShadowSettings)
            {
                ShaderPropertyOptimizer(_ToggleSteps, null, null, "Toggle SCS", "Toggles the Step based Cel Shading system: Removes the Toon Ramp and replaces it with a Step system");
                ShaderPropertyOptimizer(_ShadowMask, _ShadowMaskStrength, _ShadowMaskTexChannelSwitch, "Shadow Mask", "Texture = Black levels remove Shadows, White parts will leave Shadows as is.\nFirst Channel for Pixel Lights\nSecond Channel for Indirect Baked Light\nFirst Property = Strength\nSecond Property = Texture Channel Switch");
                ShaderPropertyOptimizer(_OcclusionMap, _OcclusionStrength, _OcclusionMapUVSwitch, "Occlusion", "Occlusion Mapping (The occlusion map is used to provide information about which areas of the model should receive high or low indirect lighting, including Cubemap Reflections)");

                Moristyles.PartingLine();

                if (_ToggleSteps.floatValue == 0)
                {
                    ShaderPropertyOptimizer(_ToonRamp, _RampOffset, null, "Shadow Ramp", "Toon Ramp, Dark to Light should be Left to Right, or Down to Up. Shadow Color is used via the Colors (Alpha) value.");
                }
                else
                {
                    ShaderPropertyOptimizer(_Steps, null, null, "Amount of Steps", "Set the amount of Steps in Int range");
                    ShaderPropertyOptimizer(_RampOffset, null, null, "Shadow Offset", "Offsets the Shadows");
                }
                ShaderPropertyOptimizer(_NdLHalfingControl, null, null, "Shadow Squeeze", "Squeezes NdL to bring the shadow more to the middle");

                Moristyles.PartingLine();

                ShaderPropertyOptimizer(_RampColor, null, null, "Shadow Color", "Change the Color of the casted Shadows on your Mesh");
                ShaderPropertyOptimizer(_ShadowColorMap, _ShadowColorMapStrength, null, "Shadow Color Map", "Texture used to define parts of your Mesh Shadows to be a different Color\nRequires the Color Strength to be non 0");
                ShaderPropertyOptimizer(_ColoringPointLights, null, null, "Color Strength Point/Spot Lights", "This Setting will apply the Shadow Color for Point/Spot Lights\nYou probably want to keep this setting fairly low since Point and Spot Lights do still over brighten with the Light Limiter being active\nThis setting will affect ALL Point/Spot Lights which are set to either 'Not Important' or 'Important'");
                ShaderPropertyOptimizer(_ColoringDirectEnvLights, null, null, "Color Strength Direct/Ambient Lights", "This Setting will apply the Shadow Color for Directional Lights and Enviromental/Ambient Lighting\nThis also counts for Directional Lights which are set to 'Not Important' since those will just contribute to the Enviromental/Ambient Lighting");
                if (_AdvancedExperimentalToggle.floatValue != 0)
                {
                    Moristyles.PartingLine();

                    ShaderPropertyOptimizer(_DirectShadowIntensity, null, null, "Direct Shadow Intensity", "Controls the Opacity of the Shadows which are casted on the Mesh from Directional Light Sources.\nYou probably want to scale this property equally to the (Shadow Caster Intensity) since it will first of all have the same Intensity and it will fix the Problem with the Shadow Casters Shadow producing edge artifacts on its own in most cases.");
                    ShaderPropertyOptimizer(_SelfCastShadows, null, null, "Shadow Caster Intensity", "Controls the Opacity of the Shadows which are casted on the Mesh from other Meshes in between your Mesh and Directional Light Sources which are set to 'Important'.\nYou can also deactivate these Receiving shadows from the Shadow Caster by disabling them in your Mesh Renderer.\nYou probably want to scale this property equally with the (Direct Shadow Intensity)!\nNOTE that this setting will have no effect on Pointlights with activated Shadow casting!");
                    ShaderPropertyOptimizer(_PointSpotShadowIntensity, null, null, "Point/Spot Shadow Intensity", "Controls the Opacity of Point and Spot Lights that are either set to 'Important' or 'Not Important'.");
                    ShaderPropertyOptimizer(_IndirectShadowIntensity, null, null, "Indirect Shadow Intensity", "Controls the Opacity of the Shadows which are casted on the Mesh from Indirect Light Sources such as Baked Lightprobes Skybox/Ambient/Environment Light and Directional Lights that are set to 'Not Important'.");

                    Moristyles.PartingLine();

                    ShaderPropertyOptimizer(_LightLimiter, null, null, "Light Limiter", "This Toggle will ''Clamp'' the overall impacting Light for calculating the Base Lightingmodel.\nThis also includes Flipbooks\nExcluded from the ''clamping'' inside the Base Lighting model are Point/Spotlights");
                    ShaderPropertyOptimizer(_MaxLightDirect, null, null, "Maximum Light Multiplier", "This value will reduce the overall impacting Light for the Base Lightingmodel and Flipbooks.\n(IT IS NOT RECOMMENDED TO TOUCH THIS VALUE UNLESS YOU KNOW WHAT YOU ARE DOING. !!!USE WITH CAUTION!!!)");
                    ShaderPropertyOptimizer(_AmbientBoost, null, null, "Ambient Light Boost", "This value will increase low frequency Ambient Light.\nA value of 1 will represent pure default Ambient light, it scales linearly upon pure Ambient Light from boosted value towards pure Ambient Light till it reaches 1");
                    ShaderPropertyOptimizer(_ToggleMonochromePixelLight, null, null, "Monochrome Direct/Point/Spot Lights", "This Toggle will convert any Directional(set to 'important')/Point/Spot Lights color information into Monochrome Color Data");
                    ShaderPropertyOptimizer(_ToggleMonochromeEnv, null, null, "Monochrome Ambient Light", "This Toggle will convert Enviromental/Ambient Color information into Monochrome Color Data\nThis also applies to Directional Lights that are set to 'Not Important'");
                    ShaderPropertyOptimizer(_DistanceFade, null, null, "Distance Fade (min,max,strength(0-1))", "This setting will define a Range of camera distance to an object to darken it\nX = min\nY = max\nZ = Strength(0-1)");
                }
            }

            if (isOutlined)
            {
                showOutlines = Moristyles.ShurikenFoldout("Outlines", showOutlines, 0);
                if (showOutlines)
                {
                    ShaderPropertyOptimizer(_OutlineMode, null, null, "Outline Mode", "Controls the Outline whether it should be Light scaled or Emissive");
                    ShaderPropertyOptimizer(_OutlineWidth, null, null, "Outline Width", "Controls the Width of the Outlines");
                    ShaderPropertyOptimizer(_OutlineColor, null, null, "Outline Color", "Controls the Color of the Outlines");

                    Moristyles.PartingLine();

                    ShaderPropertyOptimizer(_OutlineTint, null, null, "Outline Tint", "Multiplies the Outline Color with the Main Texture");
                    ShaderPropertyOptimizer(_OutlineHueSpeed, null, null, "Hue Speed", "If the Outline Color is increased, the Hue Shift becomes visible.\nThis Setting will be deactivated entirely when set to 0.");
                    ShaderPropertyOptimizer(_OutlineDepthFadeDistance, null, null, "Distance Fade", "Fades out at a set Distance to remove the Outline when it gets too pixelated\nThis Setting will be deactivated entirely when set to 0");
                    ShaderPropertyOptimizer(_OutlineDistancethickening, null, null, "Distance Thickening", "Sizes the Outlines according to the distance of the Clip Position\nThis Setting will be deactivated entirely when set to 0");
                    ShaderPropertyOptimizer(_OutlineMask, _OutlineMaskUVSwitch, null, "Outline Mask", "Mask the Parts that you dont want to get affected by the Outline");
                    if (_AdvancedExperimentalToggle.floatValue != 0)
                    {
                        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                        {
                            materialEditor.TextureScaleOffsetProperty(_OutlineMask);
                        }
                    }
                }
            }
            
            showNormals = Moristyles.ShurikenFoldout("Normals", showNormals, 0);
            if (showNormals)
            {
                if (_BumpMap.textureValue == null)
                {
                    using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                    {
                        materialEditor.TexturePropertySingleLine(new GUIContent("Normal Map", "Texture Slot for the Normal Map"), _BumpMap);
                        material.SetFloat("_BumpScale", 0);
                    }
                }
                else
                    ShaderPropertyOptimizer(_BumpMap, _BumpScale, _BumpMapUVSwitch, "Normal Map", "Texture Slot for the Normal Map");
                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                {
                    materialEditor.TextureScaleOffsetProperty(_BumpMap);
                }

                Moristyles.PartingLine();

                if (_DetailNormalMap.textureValue == null)
                {
                    using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                    {
                        materialEditor.TexturePropertySingleLine(new GUIContent("Secondary Normal Map", "Texture Slot for the Secondary Normal Map"), _DetailNormalMap);
                        material.SetFloat("_DetailNormalMapScale", 0);
                    }
                }
                else
                    ShaderPropertyOptimizer(_DetailNormalMap, _DetailNormalMapScale, _DetailNormalMapUVSwitch, "Secondary Normal Map", "Texture Slot for the Secondary Normal Map");
                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                {
                    materialEditor.TextureScaleOffsetProperty(_DetailNormalMap);
                }

                Moristyles.PartingLine();

                ShaderPropertyOptimizer(_DetailMask, _DetailMaskUVSwitch, _DetailMaskTexChannelSwitch, "Normal Mask", "Mask the Parts that you dont want to get affected by the Secondary Normal Map.\nFirst Channel for Secondary Normals\nSecond Channel for Base Normals");
                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                {
                    materialEditor.TextureScaleOffsetProperty(_DetailMask);
                }
            }

            showEmission = Moristyles.ShurikenFoldout("Emission", showEmission, 0);
            if (showEmission)
            {
                ShaderPropertyOptimizer(_EmissionMap, _EmissionColor, _EmissionMapUVSwitch, "Emission Texture", "Texture Slot is a basic Emission Slot for any sort of Texture you want to have lighten up even in the dark.(Note: Black Values in the Texture do not get rendered)");
                ShaderPropertyOptimizer(_EmissionTint, null, null, "Emission Tint", "Controls the Tint to Emission by multiplying the emission with the maintexture");
                ShaderPropertyOptimizer(_EmissionLightscale, null, null, "Toggle Light Scale", "This Toggle will make the Emission scale of the opposite way of the impacting light\ni.e. The less Light will impact your Mesh the brighter the Emission gets (No Light means full Brightness).");
                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                {
                    materialEditor.TextureScaleOffsetProperty(_EmissionMap);
                }
            }

            //For Advanced version of the Shader
            if (_COLORCOLOR.floatValue == 1)
            {
                showReflections = Moristyles.ShurikenFoldout("Reflections", showReflections, 0);
                if (showReflections)
                {
                    ShaderPropertyOptimizer(_GlossyReflections, null, null, "Toggle Cubemap", "Toggles the Cubemap on or off");
                    if (_GlossyReflections.floatValue == 1) // CUBEMAP
                    {
                        ShaderPropertyOptimizer(_WorkflowSwitch, null, null, "Workflow Switch", "Switches the used Workflow depending on what you want to use. The Dynamic Workflow gives you the best of both Workflows and will switch automatically to a selected Baked Cubemap when the Shader wont find any Reflection Probes nearby which happens in realtime");
                        ShaderPropertyOptimizer(_ToggleDisneyDiffuse, null, null, "Disney Diffuse Term", "Toggles the Disney Diffuse Term on or off, this will replace the Toon Ramp\nDisney diffuse is a Term to describe the diffuse part of a material to be coherent with the specular term and take into account the surface roughness\nOr in other Words, this will apply Standard Shader Diffuse Lighting but also for all Lightsources(Pixel, Vertex and Ambient)");
                        ShaderPropertyOptimizer(_BRDFReflInheritAniso, null, null, "Inherit Anisotropy", "Inherits Anisotropic bent Normals from the GGX Anisotropic Specular Highlight section\nThis will also apply the bent Normals to the Rimlight Indirect Spec Lights to make computation cheaper and to get a consistent view on the material since it doesn't make sense if the material is anisotropic in nature but somehow would be able to be anisotropic and also be isotropic.");
                        ShaderPropertyOptimizer(_IgnoreNormalsCubemap, null, null, "Ignore Normal Maps", "Forces the Cubemap to Ignore Normal Maps for a smooth surface reflection");
                        if (_SpecularHighlights.floatValue == 0)
                        {
                            ShaderPropertyOptimizer(_SpecularHighlights, null, null, "Specular Highlights", "Enables or Disables the Specular Highlights in all Workflows");
                        }
                        else
                        {
                            Moristyles.PartingLine();

                            ShaderPropertyOptimizer(_SpecularHighlights, null, null, "Specular Highlights", "Enables or Disables the Specular Highlights in all Workflows");
                            ShaderPropertyOptimizer(_SpecShadowMaskVar, null, null, "Shadow Masking Variant", "Switches the Shadow masking Style or even deactivates the Shadow contribution");
                            if (_SpecShadowMaskVar.floatValue == 2) 
                                ShaderPropertyOptimizer(_SpecShadowMaskPower, null, null, "Shadow Masking Power", "Controls how much the Shadow will contribute to the Specular Highlights\nthe Gitlab Wiki goes into great depth with this setting, make sure to use it wisely!");
                            if (_SpecularHighlights.floatValue == 1 && _EnableGSAA.floatValue == 0) Moristyles.PartingLine();
                        }

                        if (_EnableGSAA.floatValue == 0)
                        {
                            ShaderPropertyOptimizer(_EnableGSAA, null, null, "Enable GSAA", "Enables or Disables the Geometric Specular Anti Aliasing Feature\nTHIS FEATURE HAS A PROFOUND VISUAL IMPACT");
                        }
                        else
                        {
                            Moristyles.PartingLine();

                            ShaderPropertyOptimizer(_EnableGSAA, null, null, "Enable GSAA", "Enables or Disables the Geometric Specular Anti Aliasing Feature\nTHIS FEATURE HAS A PROFOUND VISUAL IMPACT");
                            ShaderPropertyOptimizer(_GSAAVariance, null, null, "GSAA Variance", "Increases or lowers the Variance of the GSAA effect");
                            ShaderPropertyOptimizer(_GSAAThreshold, null, null, "GSAA Threshold", "Increases or lowers the Threshold of the GSAA effect");
                        }

                        Moristyles.PartingLine();
                        
                        materialEditor.TexturePropertySingleLine(new GUIContent("Baked Cubemap", "Baked Cubemap."), _Cubemap);
                        ShaderPropertyOptimizer(_MetallicGlossMap, _Metallic, null, "Metallic", "Metallic Map and Modifier.");
                        ShaderPropertyOptimizer(_CubemapIntensity, null, null, "Cubemap Intensity", "controls the Cubemap Intensity");
                        ShaderPropertyOptimizer(_Glossiness, null, null, "Smoothness", "Cubemap smoothness, blurs the Cubemap");
                        ShaderPropertyOptimizer(_MetallicGlossMapUVSwitch, null, null, "Metallic Map UV", "Metallic Map UV Switch");
                        ShaderPropertyOptimizer(_ReflectionMask, _ReflectionMaskUVSwitch, _ReflectionMaskTexChannelSwitch, "Reflection Mask", "Mask the Parts that you dont want to get affected by the Reflection.\nSecond Property = UV Switch\nThird Property = Texture Channel Switch");
                        if (_AdvancedExperimentalToggle.floatValue != 0)
                        {
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_ReflectionMask);
                            }
                        }
                    }

                    Moristyles.PartingLine();

                    showMatcapR1 = Moristyles.ShurikenFoldout("Matcap1 <color=#ff0000ff> [Mask = Red Channel] </color>", showMatcapR1, Moristyles.shurikenIndent);
                    if (showMatcapR1)
                    {
                        ShaderPropertyOptimizer(_MatcapR1Toggle, null, null, "Matcap Toggle", "Toggles the Matcap on or off");
                        if (_MatcapR1Toggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_MatcapR1Mode, null, null, "Matcap Blending Mode", "Matcap Blending Mode");
                            ShaderPropertyOptimizer(_MatcapViewDir, null, null, "Matcap Projection", "Changes the Matcap Projection to either Xiexes Singularity or d4rkpl4y3rs Orthographic Projection Method, both have its pros and cons and neither is perfect so choose the one you like the best basically");
                            ShaderPropertyOptimizer(_IgnoreNormalsMatcap, null, null, "Ignore Normal Maps", "Forces the Matcap to Ignore Normal Maps for a smooth surface reflection");
                            ShaderPropertyOptimizer(_MatcapR1, _MatcapR1Color, null, "Matcap", "Matcap Texture");
                            ShaderPropertyOptimizer(_MatcapR1Blending, null, null, "Matcap Blending", "Matcap Blending, ");
                            ShaderPropertyOptimizer(_MatcapR1Intensity, null, null, "Reflection Intensity", "controls the Intensity of the chosen Reflectiontype");
                            if (_MatcapR1Mode.floatValue != 0)
                                ShaderPropertyOptimizer(_MatcapR1Tint, null, null, "Reflection Tint", "Reflection Tint to Main Texture");
                            ShaderPropertyOptimizer(_MatcapR1smoothness, null, null, "Matcap smoothness", "Matcap smoothness, blurs the Matcap");
                            ShaderPropertyOptimizer(_MatcapMask, _MatcapR1MaskInvert, _MatcapMaskUVSwitch, "Reflection Mask", "Mask the Parts that you dont want to get affected by the Matcap\nToggle = Texture Inverter per Channel");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_MatcapMask);
                                }
                            }
                        }
                    }

                    showMatcapG2 = Moristyles.ShurikenFoldout("Matcap2 <color=#00ff00ff> [Mask = Green Channel] </color>", showMatcapG2, Moristyles.shurikenIndent);
                    if (showMatcapG2)
                    {
                        ShaderPropertyOptimizer(_MatcapG2Toggle, null, null, "Matcap Toggle", "Toggles the Matcap on or off");
                        if (_MatcapG2Toggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_MatcapG2Mode, null, null, "Matcap Blending Mode", "Matcap Blending Mode");
                            ShaderPropertyOptimizer(_MatcapViewDir, null, null, "Matcap Projection", "Changes the Matcap Projection to either Xiexes Singularity or d4rkpl4y3rs Orthographic Projection Method, both have its pros and cons and neither is perfect so choose the one you like the best basically");
                            ShaderPropertyOptimizer(_IgnoreNormalsMatcap, null, null, "Ignore Normal Maps", "Forces the Matcap to Ignore Normal Maps for a smooth surface reflection");
                            ShaderPropertyOptimizer(_MatcapG2, _MatcapG2Color, null, "Matcap", "Matcap Texture");
                            ShaderPropertyOptimizer(_MatcapG2Blending, null, null, "Matcap Blending", "Matcap Blending");
                            ShaderPropertyOptimizer(_MatcapG2Intensity, null, null, "Reflection Intensity", "controls the Intensity of the chosen Reflection Type");
                            if (_MatcapG2Mode.floatValue != 0)
                                ShaderPropertyOptimizer(_MatcapG2Tint, null, null, "Reflection Tint", "Reflection Tint to Main Texture");
                            ShaderPropertyOptimizer(_MatcapG2smoothness, null, null, "Matcap smoothness", "Matcap smoothness, blurs the Matcap");
                            ShaderPropertyOptimizer(_MatcapMask, _MatcapG2MaskInvert, _MatcapMaskUVSwitch, "Reflection Mask", "Mask the Parts that you dont want to get affected by the Matcap\nToggle = Texture Inverter per Channel");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_MatcapMask);
                                }
                            }
                        }
                    }

                    showMatcapB3 = Moristyles.ShurikenFoldout("Matcap3 <color=#0000ffff> [Mask = Blue Channel] </color>", showMatcapB3, Moristyles.shurikenIndent);
                    if (showMatcapB3)
                    {
                        ShaderPropertyOptimizer(_MatcapB3Toggle, null, null, "Matcap Toggle", "Toggles the Matcap on or off");
                        if (_MatcapB3Toggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_MatcapB3Mode, null, null, "Matcap Blending Mode", "Matcap Blending Mode");
                            ShaderPropertyOptimizer(_MatcapViewDir, null, null, "Matcap Projection", "Changes the Matcap Projection to either Xiexes Singularity or d4rkpl4y3rs Orthographic Projection Method, both have its pros and cons and neither is perfect so choose the one you like the best basically");
                            ShaderPropertyOptimizer(_IgnoreNormalsMatcap, null, null, "Ignore Normal Maps", "Forces the Matcap to Ignore Normal Maps for a smooth surface reflection");
                            ShaderPropertyOptimizer(_MatcapB3, _MatcapB3Color, null, "Matcap", "Matcap Texture");
                            ShaderPropertyOptimizer(_MatcapB3Blending, null, null, "Matcap Blending", "Matcap Blending, ");
                            ShaderPropertyOptimizer(_MatcapB3Intensity, null, null, "Reflection Intensity", "controls the Intensity of the chosen Reflectiontype");
                            if (_MatcapB3Mode.floatValue != 0)
                                ShaderPropertyOptimizer(_MatcapB3Tint, null, null, "Reflection Tint", "Reflection Tint to Main Texture");
                            ShaderPropertyOptimizer(_MatcapB3smoothness, null, null, "Matcap smoothness", "Matcap smoothness, blurs the Matcap");
                            ShaderPropertyOptimizer(_MatcapMask, _MatcapB3MaskInvert, _MatcapMaskUVSwitch, "Reflection Mask", "Mask the Parts that you dont want to get affected by the Matcap\nToggle = Texture Inverter per Channel");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_MatcapMask);
                                }
                            }
                        }
                    }

                    showMatcapA4 = Moristyles.ShurikenFoldout("Matcap4 <color=#ffffffff> [Mask = Alpha Channel] </color>", showMatcapA4, Moristyles.shurikenIndent);
                    if (showMatcapA4)
                    {
                        ShaderPropertyOptimizer(_MatcapA4Toggle, null, null, "Matcap Toggle", "Toggles the Matcap on or off");
                        if (_MatcapA4Toggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_MatcapA4Mode, null, null, "Matcap Blending Mode", "Matcap Blending Mode");
                            ShaderPropertyOptimizer(_MatcapViewDir, null, null, "Matcap Projection", "Changes the Matcap Projection to either Xiexes Singularity or d4rkpl4y3rs Orthographic Projection Method, both have its pros and cons and neither is perfect so choose the one you like the best basically");
                            ShaderPropertyOptimizer(_IgnoreNormalsMatcap, null, null, "Ignore Normal Maps", "Forces the Matcap to Ignore Normal Maps for a smooth surface reflection");
                            ShaderPropertyOptimizer(_MatcapA4, _MatcapA4Color, null, "Matcap", "Matcap Texture");
                            ShaderPropertyOptimizer(_MatcapA4Blending, null, null, "Matcap Blending", "Matcap Blending, ");
                            ShaderPropertyOptimizer(_MatcapA4Intensity, null, null, "Reflection Intensity", "controls the Intensity of the chosen Reflectiontype");
                            if (_MatcapA4Mode.floatValue != 0)
                                ShaderPropertyOptimizer(_MatcapA4Tint, null, null, "Reflection Tint", "Reflection Tint to Main Texture");
                            ShaderPropertyOptimizer(_MatcapA4smoothness, null, null, "Matcap smoothness", "Matcap smoothness, blurs the Matcap");
                            ShaderPropertyOptimizer(_MatcapMask, _MatcapA4MaskInvert, _MatcapMaskUVSwitch, "Reflection Mask", "Mask the Parts that you dont want to get affected by the Matcap\nToggle = Texture Inverter per Channel");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_MatcapMask);
                                }
                            }
                        }
                    }
                    Moristyles.PartingLine();
                }

                showSpecular = Moristyles.ShurikenFoldout("Specular Highlights", showSpecular, 0);
                if (showSpecular)
                {
                    ShaderPropertyOptimizer(_SpecularToggle, null, null, "Toggle Specular", "Specular On, Off");
                    if (_SpecularToggle.floatValue == 1) // On
                    {
                        ShaderPropertyOptimizer(_SpecularSetting, null, null, "Specular Control", "Toon, GGX or Anisotropic Specular");
                        ShaderPropertyOptimizer(_SpecShadowMaskVar, null, null, "Shadow Masking Variant", "Switches the Shadow masking Style or even deactivates the Shadow contribution");
                        if (_SpecShadowMaskVar.floatValue == 2) 
                            ShaderPropertyOptimizer(_SpecShadowMaskPower, null, null, "Shadow Masking Power", "Controls how much the Shadow will contribute to the Specular Highlights\nthe Gitlab Wiki goes into great depth with this setting, make sure to use it wisely!");
                        if (_EnableGSAA.floatValue == 0)
                        {
                            ShaderPropertyOptimizer(_EnableGSAA, null, null, "Enable GSAA", "Enables or Disables the Geometric Specular Anti Aliasing Feature\nTHIS FEATURE HAS A PROFOUND VISUAL IMPACT");
                        }
                        else
                        {
                            Moristyles.PartingLine();

                            ShaderPropertyOptimizer(_EnableGSAA, null, null, "Enable GSAA", "Enables or Disables the Geometric Specular Anti Aliasing Feature\nTHIS FEATURE HAS A PROFOUND VISUAL IMPACT");
                            ShaderPropertyOptimizer(_GSAAVariance, null, null, "GSAA Variance", "Increases or lowers the Variance of the GSAA effect");
                            ShaderPropertyOptimizer(_GSAAThreshold, null, null, "GSAA Threshold", "Increases or lowers the Threshold of the GSAA effect");
                        }

                        Moristyles.PartingLine();

                        if (_SpecularSetting.floatValue == 0) // Toon
                        {
                            ShaderPropertyOptimizer(_SpecularMap, _SpecularColor, _SpecularMapUVSwitch, "Specular Map", "The Texture that tells the Specular Highlights where to render and where it shouldnt");
                            ShaderPropertyOptimizer(_SpecularTint, null, null, "Specular Tint", "Controls the Tint from Specular to Maintexture by multiplying the Speculars Color with the maintexture");
                            ShaderPropertyOptimizer(_HighlightOffset, null, null, "Specular Offset", "Offsets the Specular Range.");
                            ShaderPropertyOptimizer(_HighlightSmoothness, null, null, "Specular Smoothness", "Controls the smoothness factor of the Specular Highlight");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_SpecularMap);
                                }
                            }
                        }
                        else if (_SpecularSetting.floatValue == 1) // Standard
                        {
                            ShaderPropertyOptimizer(_SpecularMap, _SpecularColor, _SpecularMapUVSwitch, "Specular Map", "The Texture that tells the Specular Highlights where to render and where it shouldnt");
                            ShaderPropertyOptimizer(_SpecularTint, null, null, "Specular Tint", "Controls the Tint from Specular to Maintexture by multiplying the Speculars Color with the maintexture");
                            ShaderPropertyOptimizer(_HighlightSmoothness, null, null, "Specular Smoothness", "Controls the smoothness factor of the Specular Highlight");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_SpecularMap);
                                }
                            }
                        }
                        else if (_SpecularSetting.floatValue == 2) // Anisotropic by james Ohare
                        {
                            ShaderPropertyOptimizer(_SpecularMap, _SpecularColor, _SpecularMapUVSwitch, "Specular Map", "The Texture that tells the Specular Highlights where to render and where it shouldnt");
                            ShaderPropertyOptimizer(_AnisoSharpening, null, null, "Specular Sharpening", "This Toggle imitates a Toon Style.");
                            ShaderPropertyOptimizer(_SpecularTint, null, null, "Specular Tint", "Controls the Tint from Specular to Maintexture by multiplying the Speculars Color with the maintexture");
                            ShaderPropertyOptimizer(_AnisoScale, null, null, "Specular Scale", "Offsets the Anisotropic Specular Range.\nDefault settings for Anisotropic Hightlights is 0.5");
                            ShaderPropertyOptimizer(_HighlightOffset, null, null, "Specular Offset", "Offsets the Specular Range.");
                            ShaderPropertyOptimizer(_BlinntoAniso, null, null, "Blinn to Aniso", "if there is no Specular Map defined it will blend between blinn highlights and anisotropic highlights. Can be used to push the highlight towards or away from the centre point.\nDefault settings for Anisotropic Hightlights is 0.25");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_SpecularMap);
                                }
                                Moristyles.PartingLine();
                            }
                            
                            ShaderPropertyOptimizer(_AnisoDir, _AnisoDirUVSwitch, null, "Aniso Direction", "Direction of the surface highlight. Follows the same directional values as a tangent space normal map");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_AnisoDir);
                                }
                            }
                        }
                        else if (_SpecularSetting.floatValue == 3) // GGX Anisotropic
                        {
                            ShaderPropertyOptimizer(_SpecularMap, _SpecularColor, _SpecularMapUVSwitch, "Specular Map", "The Texture that tells the Specular Highlights how to render on a given texel, for GGX Anisotropic Highlights the following applies;\nR = Intensity\nG = Sharpening\nB = Anisotropy");
                            //ShaderPropertyOptimizer(_AnisoF0Reflectance, null, null, "Metallic", "f0 reflectance = Metallic, increase this value to 1 if your Material is Metallic"); //We do not work in Metallic Workflow in additional Specular Highlights
                            ShaderPropertyOptimizer(_HighlightSmoothness, null, null, "Specular Smoothness", "Controls the smoothness factor of the Specular Highlight");
                            ShaderPropertyOptimizer(_Anisotropy, null, null, "Anisotropy", "Offsets the Specular Range.\nDefault settings for Anisotropic Hightlights is -0.2");
                            ShaderPropertyOptimizer(_SpecularTint, null, null, "Specular Tint", "Controls the Tint from Specular to Maintexture by multiplying the Speculars Color with the maintexture");
                            ShaderPropertyOptimizer(_AnisoSharpening, null, null, "Specular Sharpening", "This Toggle imitates a Toon Style.");
                            ShaderPropertyOptimizer(_AnisoFlickerFix, null, null, "Flicker Fix", "This Property tries to fix the flickering issue on bad topology\n!!ONLY USE WHEN NEEDED!!");
                            if (_AnisoFlickerFix.floatValue == 1)
                                ShaderPropertyOptimizer(_AnisoFlickerFixOffset, null, null, "Flicker Fix Offset", "The amount of Offset given for the Flicker Fix\nIt is best to have this value as low as possible to a point where you wont see any major aliasing artifacts.");
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_SpecularMap);
                                }
                            }
                        }
                    }
                }

                showRimlight = Moristyles.ShurikenFoldout("Rimlight", showRimlight, 0);
                {
                    if (showRimlight)
                    {
                        ShaderPropertyOptimizer(_RimToggle, null, null, "Toggle Rimlight", "Rimlight On, Off");
                        if (_RimToggle.floatValue == 1) // On
                        {
                            ShaderPropertyOptimizer(_RimDirectionToggle, null, null, "Toggle Direction", "Toggle for using either a Light based Direction or a Standard Fresnel Effect");
                            ShaderPropertyOptimizer(_RimFaceCulling, null, null, "Face Rendering", "Choose which Face to Render the Rim Light for");
                            ShaderPropertyOptimizer(_RimSpecToggle, null, null, "Indirect Spec Light", "Adds Indirect Specular Light into the Mix");
                            if (_RimSpecToggle.floatValue == 1)
                            {
                                ShaderPropertyOptimizer(_RimSpecLightsmoothness, null, null, "Indirect Spec Light smoothness", "makes you able to smoothen the Indirect Specular Light");
                                ShaderPropertyOptimizer(_RimOpacity, null, null, "Indirect Spec Strength", "Indirect Spec Light strength, Increases the strength of the Indirect Specular Light");
                                ShaderPropertyOptimizer(_RimLightReflInheritAniso, null, null, "Inherit Anisotropy", "Inherits Anisotropic bent Normals from the Specular Highlight setting");
                            }

                            if (_RimDirectionToggle.floatValue == 1)
                            {
                                ShaderPropertyOptimizer(_RimFresnelBias, null, null, "Fresnel Bias", "This Value controls the Bias of the Fresnel Effect");
                                ShaderPropertyOptimizer(_RimFresnelScale, null, null, "Fresnel Scale", "This Value controls the Scale of the Fresnel Effect");
                                ShaderPropertyOptimizer(_RimFresnelPower, null, null, "Fresnel Power", "This Value controls the Power of the Fresnel Effect");
                            }

                            Moristyles.PartingLine();

                            ShaderPropertyOptimizer(_RimColor, null, null, "Color", "Rimlight Color");
                            ShaderPropertyOptimizer(_RimMask, _RimMaskUVSwitch, _RimMaskTexChannelSwitch, "Rim Map", "The Texture that tells the Rim Light where to render and where it shouldnt\nSecond Property = UV Switch\nThird Property = Texture Channel Switch");
                            ShaderPropertyOptimizer(_RimTint, null, null, "Tint to Albedo", "Will multiply the Rimlight with the Maintexture");
                            if (_RimDirectionToggle.floatValue == 0)
                            {
                                ShaderPropertyOptimizer(_RimOffset, null, null, "Rim Offset", "Range of the Rim");
                                ShaderPropertyOptimizer(_RimPower, null, null, "Rim Power", "Threshold of the Rim");
                            }

                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_RimMask);
                                }
                            }
                        }
                    }
                }
                showSSS = Moristyles.ShurikenFoldout("Subsurface Scattering", showSSS, 0);
                if (showSSS)
                {
                    ShaderPropertyOptimizer(_SSSToggle, null, null, "Toggle SSS", "Intensity of SSS (usually 0-1)");
                    if (_SSSToggle.floatValue == 1)
                    {
                        ShaderPropertyOptimizer(_SSSSetting, null, null, "SSS Control", "Light Controlled or Color Controlled");
                        if (_SSSSetting.floatValue != 0) // Color Based
                            ShaderPropertyOptimizer(_SSSColor, null, null, "Color", "SSS Color");
                        ShaderPropertyOptimizer(_SSSThicknessMap, _SSSThicknessinv, _SSSThicknessMapUVSwitch, "SSS Map", "Specifies the thickness of the SSS and tells the Feature how much it should pass through on which point of the Mesh\nToggle = Map inverter");
                        ShaderPropertyOptimizer(_SSSMapMode, null, null, "SSS Map Mode", "Switches the SSS Map from Color Information to Modification Data which means that the following Channel will be multiplied with the SSS properties\nRed Channel: Intensity\nGreen Channel: Power\nBlue Channel: Distortion");
                        ShaderPropertyOptimizer(_SSSTint, null, null, "Tint to Diffuse", "Tints/Multiplys the Subsurface Scattering effect with the Main Texture");
                        ShaderPropertyOptimizer(_SSSScale, null, null, "Intensity", "Intensity of SSS (usually 0-1)");
                        ShaderPropertyOptimizer(_SSSPower, null, null, "Power", "Threshold of SSS");
                        ShaderPropertyOptimizer(_SubsurfaceDistortionModifier, null, null, "Distortion", "Distortion Modifier of SSS (usually 0 for inlined or 1 for outlined");
                        if (_AdvancedExperimentalToggle.floatValue != 0)
                        {
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_SSSThicknessMap);
                            }
                        }
                    }
                }
            }

            Moristyles.PartingLine();

            showMiscellaneous = Moristyles.ShurikenFoldout("<color=#ffffffff>Miscellaneous</color>", showMiscellaneous, 0);
            if (showMiscellaneous)
            {
                showMainTexHueShift = Moristyles.ShurikenFoldout("Main Texture <color=#AA0000>H</color><color=#00AA00>U</color><color=#0000AA>E</color> Shift", showMainTexHueShift, Moristyles.shurikenIndent);
                if (showMainTexHueShift)
                {
                    ShaderPropertyOptimizer(_ToggleHueTexforSpeed, null, null, "Tex G Channel Speed", "makes you able to use the Hue Masks Green Channel for Speed, keep in mind that your Texture needs to be pin point correct or you will get artifacts ! meaning that your Texture should not have unexpected gradients nor will be affected by any sort of texture compression");
                    ShaderPropertyOptimizer(_HueMask, _HueMaskinverter, _HueMaskUVSwitch, "Hue Mask", "Texture Mask for the Hue Effect (will only affect the Hue and not the Main Texture)\nToggle = Map inverter");
                    ShaderPropertyOptimizer(_HueShiftblend, null, null, "Hue Shift blend", "Controls the Blending of the Main Texture to the Shift Operation");
                    ShaderPropertyOptimizer(_HueShiftSpeed, null, null, "Hue Shift Speed", "Controls the speed of the shift Operation");
                    ShaderPropertyOptimizer(_HueShiftRandomizer, null, null, "Hue Shift Offset", "Offsets Time by the given Value");
                    if (_AdvancedExperimentalToggle.floatValue != 0)
                    {
                        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                        {
                            materialEditor.TextureScaleOffsetProperty(_HueMask);
                        }
                    }
                }

                showShadowEmissiveRim = Moristyles.ShurikenFoldout("Shadow / Emissive Rim", showShadowEmissiveRim, Moristyles.shurikenIndent);
                if (showShadowEmissiveRim)
                {
                    ShaderPropertyOptimizer(_RimSwitch, null, null, "Rim Mode", "Switches between a Shadow Rim and Emissive Rim Mode");
                    if (_RimSwitch.floatValue == 0)
                    {
                        ShaderPropertyOptimizer(_ShadowRimOpacity, null, null, "Rim Intensity", "Controls the amount of the Shadow Rim.");
                    }
                    else
                    {
                        material.SetFloat("_ShadowRimOpacity", 1f);
                        ShaderPropertyOptimizer(_EmissiveRimColor, null, null, "Rim Color", "The Color of the Emissive Rim");
                        ShaderPropertyOptimizer(_RimHueSpeed, null, null, "Hue Speed", "If the Rim Color is increased, the Hue Shift becomes visible. When the Hue Speed is at 0 then it becomes pure white so that the Hue Shift is disabled");
                    }

                    ShaderPropertyOptimizer(_ShadowRimRange, null, null, "Rim Range", "Controls the Range of the Shadow Rim.");
                    ShaderPropertyOptimizer(_ShadowRimSharpness, null, null, "Rim Sharpness", "Controls the sharpness/smoothness of the Shadow Rim.");
                }

                showDither = Moristyles.ShurikenFoldout("Dither", showDither, Moristyles.shurikenIndent);
                if (showDither)
                {
                    ShaderPropertyOptimizer(_DitherAlphaToggle, null, null, "Dither Method", "Switch between Surface Depth or Diffuse(MainTex) Alpha as source");
                    ShaderPropertyOptimizer(_DitherTextureToggle, null, null, "Toggle Dither Texture", "Activates a Texture Slot for you to use a Texture as Pattern for the Dither Effect");
                    if (_DitherTextureToggle.floatValue == 1)
                    {
                        materialEditor.TexturePropertySingleLine(new GUIContent("Dither Pattern", "Lets you use a Texture Pattern for the Dither Effect"), _DitherTexture);
                        ShaderPropertyOptimizer(_DitherTextureTiling, null, null, "Texture Tiling", "Tiles the Dither Texture");
                    }

                    ShaderPropertyOptimizer(_StartDitheringFade, null, null, "Start Dithering Fade", "DISTANCE DITHERING: Controls the Dither Range from where to Start to Dither, so by setting this value to 1 the effect will start after having the Camera 1 Meter away from the Mesh\nDIFFUSE ALPHA: Range 0-1, default is 0.01");
                    ShaderPropertyOptimizer(_EndDitheringFade, null, null, "End Dithering Fade", "DISTANCE DITHERING: Controls the Dither Range from where to End to Dither, so by setting this value to 1 the effect will start before/inside a Camera Range between 0 and 1 apart from the Mesh\nDIFFUSE ALPHA: Range 0-1, default is 0.99");
                    ShaderPropertyOptimizer(_DitherMask, _DitherMaskUVSwitch, null, "Dither Mask", "Lets you use decide where to draw(value = 1) or not to draw(value = 0) the Dither Effect");
                    if (_AdvancedExperimentalToggle.floatValue != 0)
                    {
                        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                        {
                            materialEditor.TextureScaleOffsetProperty(_DitherMask);
                        }
                    }
                }

                //Old Dissolve System
                //showDissolve = Moristyles.ShurikenFoldout("Dissolve", showDissolve, Moristyles.shurikenIndent);
                //if (showDissolve)
                //{
                //    ShaderPropertyOptimizer(_DissolveModifier, null, null, "Dissolve Modifier", "This is the actual Dissolving effect which will dissolve the parts that are defined in the (Main Dissolve Mask or via Vertex Colors) keep in mind that the Colors strength needs to be above the Mask Clip Value which is 0.5 so everything with a Color strength under 50% will not be visible and everything above 50% will be visible.\nLOWERING THIS SETTING ONCE BELOW 1 WILL ACTIVATE THE IGNOREPROJECTOR TAG IN THE RENDERING OPTIONS SECTION AND WILL NOT AUTOMATICALLY SWITCH BACK TO DEFAULT STATE AFTERWARDS");
                //    if (_DissolveModifier.floatValue == -1)
                //        ShaderPropertyOptimizer(_DissolveRemapMin, null, null, "Remap Min", "Remaps the boundaries for the Dissolve and Materialize effect.\nSet this Value to the point where you cannot see your mesh anymore + a little extra");
                //    else if (_DissolveModifier.floatValue == 1) ShaderPropertyOptimizer(_DissolveRemapMax, null, null, "Remap Max", "Remaps the boundaries for the Dissolve and Materialize effect.\nSet this Value to the point where you can see your entire Mesh + a little extra");
                //    if (_MaterializeVertexColor.floatValue == 0)
                //        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                //        {
                //            materialEditor.TexturePropertySingleLine(new GUIContent("Main Dissolve Mask", "This is The Base Texture which defines the main parts and can be used to dissolve certain parts of your model or your entire model(by default when there is no texture defined)"), _DissolveMask);
                //        }
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_DissolveDensity, null, null, "Effect Density", "Lets you choose how dense the effect of the Pattern will be. The lower the Value the more dense the effect will be");
                //    using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                //    {
                //        materialEditor.TexturePropertySingleLine(new GUIContent("Effect Pattern", "Lets you use a Texture Pattern for the Dissolve/Materialize Effect"), _DissolvePattern);
                //    }
                //
                //    ShaderPropertyOptimizer(_DissolvePatternUVSwitch, null, null, "UV", "Switches the UV Channel on the upper Texture sample");
                //    using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                //    {
                //        materialEditor.TextureScaleOffsetProperty(_DissolvePattern);
                //    }
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_MaterializeLayerModeR, null, null, "Layer Mode Red", "This controls how the Color Layers interact with each other\nSeparate = each Layer Separated\nMerged = Merge from 0 - 100\nReverse Merged = Merge from 100 - 0");
                //    ShaderPropertyOptimizer(_MaterializeColorLayerR, null, null, "Color Layer Red", "This setting will change the Layer you are working on, depending on the Color Intensity.\nFor Example if you paint an Object with .65% Red then you can switch to Layer 65 and work with it there. (Max = 100)");
                //    ShaderPropertyOptimizer(_MaterializeR, null, null, "Materialize R", "This is the Materialize Slider which lets you reappear parts of your Mesh which are defined in the\n(Materialize Texture RED CHANNEL)");
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_MaterializeLayerModeG, null, null, "Layer Mode Green", "This controls how the Color Layers interact with each other\nSeparate = each Layer Separated\nMerged = Merge from 0 - 100\nReverse Merged = Merge from 100 - 0");
                //    ShaderPropertyOptimizer(_MaterializeColorLayerG, null, null, "Color Layer Green", "This setting will change the Layer you are working on, depending on the Color Intensity.\nFor Example if you paint an Object with .65% Red then you can switch to Layer 65 and work with it there. (Max = 100)");
                //    ShaderPropertyOptimizer(_MaterializeG, null, null, "Materialize G", "This is the Materialize Slider which lets you reappear parts of your Mesh which are defined in the\n(Materialize Texture GREEN CHANNEL)");
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_MaterializeLayerModeB, null, null, "Layer Mode Blue", "This controls how the Color Layers interact with each other\nSeparate = each Layer Separated\nMerged = Merge from 0 - 100\nReverse Merged = Merge from 100 - 0");
                //    ShaderPropertyOptimizer(_MaterializeColorLayerB, null, null, "Color Layer Blue", "This setting will change the Layer you are working on, depending on the Color Intensity.\nFor Example if you paint an Object with .65% Red then you can switch to Layer 65 and work with it there. (Max = 100)");
                //    ShaderPropertyOptimizer(_MaterializeB, null, null, "Materialize B", "This is the Materialize Slider which lets you reappear parts of your Mesh which are defined in the\n(Materialize Texture BLUE CHANNEL)");
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_MaterializeLayerModeA, null, null, "Layer Mode Alpha", "This controls how the Color Layers interact with each other\nSeparate = each Layer Separated\nMerged = Merge from 0 - 100\nReverse Merged = Merge from 100 - 0");
                //    ShaderPropertyOptimizer(_MaterializeColorLayerA, null, null, "Color Layer Alpha", "This setting will change the Layer you are working on, depending on the Color Intensity.\nFor Example if you paint an Object with .65% Red then you can switch to Layer 65 and work with it there. (Max = 100)");
                //    ShaderPropertyOptimizer(_MaterializeA, null, null, "Materialize A", "This is the Materialize Slider which lets you reappear parts of your Mesh which are defined in the\n(Materialize Texture ALPHA CHANNEL)");
                //    if (_MaterializeVertexColor.floatValue == 0)
                //        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                //        {
                //            materialEditor.TexturePropertySingleLine(new GUIContent("Materialize Texture", "This is The Materialize Texture which defines the secondary parts to lerp to which is separated in R, G, B Channels and there Sliders"), _MaterializeTex);
                //        }
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_ToggleDissolveDirInv, null, null, "Dissolve direction inverter", "This lets you invert the direction for the\n(Main Dissolve Mask)");
                //    ShaderPropertyOptimizer(_ToggleMaterializeDirInv, null, null, "Materialize direction inverter", "This lets you invert the direction for the\n(Materialize Texture R,G,B CHANNEL)");
                //
                //    Moristyles.PartingLine();
                //
                //    ShaderPropertyOptimizer(_MaterializeVertexColor, null, null, "Use Vertex Colors", "Replaces the Materialize Texture and the Main Dissolve Mask with Vertex Colors.\nIN ADDITION TO THE MAIN DISSOLVE MASK YOU CAN DEFINE PARTS THAT SHOULD NOT BE DISSOLVED BY PAINTING THEM PURE WHITE RGB(1, 1, 1)");
                //    ShaderPropertyOptimizer(_ToggleDissolveEmission, null, null, "Toggle Emissive Effect", "Activates a Emissive Effect for Dissolve and Materializing");
                //    if (_ToggleDissolveEmission.floatValue == 1) ShaderPropertyOptimizer(_EmissiveDissolveColor, null, null, "Emissive Color", "The Color for the Emissive Effect");
                //    ShaderPropertyOptimizer(_ToggleDissolveVertexOffset, null, null, "Toggle Vertex Effect", "Activates a Vertex Offset Effect for Dissolve and Materializing");
                //    if (_ToggleDissolveVertexOffset.floatValue == 1) ShaderPropertyOptimizer(_DissolveVertexMultiplier, null, null, "Vertex Multiplier", "Increases the effect of the Vertex Normal multiplication");
                //}
                
                //Inventory System
                showInventory = Moristyles.ShurikenFoldout("Inventory", showInventory, Moristyles.shurikenIndent);
                if (showInventory)
                {
                    EditorGUI.BeginChangeCheck();
                    ShaderPropertyOptimizer(_UseInventory, null, null, "Toggle Inventory System", "");
                    if (EditorGUI.EndChangeCheck() && _UseInventory.floatValue == 1)
                        material.SetInt("_IgnoreProjector", 1); //Force no Projector receiving when the Mesh is not rendered anyway or at least part of it isn't
                    
                    if (_UseInventory.floatValue == 1)
                    {
                        using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                        {
                            materialEditor.ShaderProperty(_InventoryComplexity, new GUIContent("Complexity Switch", "Switch between simple and complex\nComplex is much more performance intense and therefore should only be used for animations to blend over to another material that has this setting set to simple!\nComplex provides visual dissolving and materializing effects\nSimple costs next to no performance and should be used most if not all the time"));
                        }
                        if (_InventoryComplexity.floatValue == 1)
                            materialEditor.ShaderProperty(_InventoryComplexityTooltip, new GUIContent("YOU HAVE THE COMPLEX INVENTORY MODE ACTIVATED\nDO NOT USE THIS MATERIAL FOR DAILY USE DUE TO A MAJOR PERFORMANCE HIT\nComplex mode is meant for animations to blend to another material which is then set to simple", ""));
                        ShaderPropertyOptimizer(_InventoryStride, null, null, "Inventory Stride", "");
                        materialEditor.ShaderProperty(_InventoryRenameProperties, new GUIContent("Rename Properties", ""));
                        Moristyles.PartingLine();
                        SetupInventoryProperty(_InventoryItem01Animated, "_InventoryItem01Name", "1");
                        SetupInventoryProperty(_InventoryItem02Animated, "_InventoryItem02Name", "2");
                        SetupInventoryProperty(_InventoryItem03Animated, "_InventoryItem03Name", "3");
                        SetupInventoryProperty(_InventoryItem04Animated, "_InventoryItem04Name", "4");
                        SetupInventoryProperty(_InventoryItem05Animated, "_InventoryItem05Name", "5");
                        SetupInventoryProperty(_InventoryItem06Animated, "_InventoryItem06Name", "6");
                        SetupInventoryProperty(_InventoryItem07Animated, "_InventoryItem07Name", "7");
                        SetupInventoryProperty(_InventoryItem08Animated, "_InventoryItem08Name", "8");
                        SetupInventoryProperty(_InventoryItem09Animated, "_InventoryItem09Name", "9");
                        SetupInventoryProperty(_InventoryItem10Animated, "_InventoryItem10Name", "10");
                        SetupInventoryProperty(_InventoryItem11Animated, "_InventoryItem11Name", "11");
                        SetupInventoryProperty(_InventoryItem12Animated, "_InventoryItem12Name", "12");
                        SetupInventoryProperty(_InventoryItem13Animated, "_InventoryItem13Name", "13");
                        SetupInventoryProperty(_InventoryItem14Animated, "_InventoryItem14Name", "14");
                        SetupInventoryProperty(_InventoryItem15Animated, "_InventoryItem15Name", "15");
                        SetupInventoryProperty(_InventoryItem16Animated, "_InventoryItem16Name", "16");
                        Moristyles.PartingLine();
                        ShaderPropertyOptimizer(_InventoryUVSwitch, null, null, "Inventory UV Switch", "This does not only change the UVs for the Dissolve Patter Texture but also for the entire feature");
                        if (_InventoryComplexity.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_EmissiveDissolveColor, null, null, "Emissive Dissolve Color", "The Color for the Dissolve/Materialize Item transition");
                            ShaderPropertyOptimizer(_InventoryEmissionThickness, null, null, "Inventory Emission Thickness", "Changes the thickness of the Dissolve/Materialize effect");
    
                            materialEditor.TexturePropertySingleLine(new GUIContent("Dissolve Pattern", ""), _DissolvePattern);
                            if (_AdvancedExperimentalToggle.floatValue != 0)
                            {
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                {
                                    materialEditor.TextureScaleOffsetProperty(_DissolvePattern);
                                }
                            }
                        }
                    }
                }
                
                
                // FOR ADVANCED ONLY
                if (_COLORCOLOR.floatValue == 1)
                {
                    showEmissionScroll = Moristyles.ShurikenFoldout("EmissionScroll", showEmissionScroll, Moristyles.shurikenIndent);
                    if (showEmissionScroll)
                    {
                        ShaderPropertyOptimizer(_EmissionScrollToggle, null, null, "Toggle EmissionScroll", "Emission Scroll On, Off");
                        if (_EmissionScrollToggle.floatValue == 1) // V1
                        {
                            ShaderPropertyOptimizer(_Emissionscroll, _EmissionscrollColor, _EmissionscrollUVSwitch, "Emission Scroll Texture", "The Main Texture for the Emission Scroll system");
                            ShaderPropertyOptimizer(_EmissionscrollTint, null, null, "Diffuse Tint", "Controls the tint to Maintex of the Emissionscroll V2 lines");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_Emissionscroll);
                            }

                            ShaderPropertyOptimizer(_Emiossionscrollspeed, null, null, "Scroll speed", "Controls the scroll speed of the Emission Scroll Texture");
                            ShaderPropertyOptimizer(_VectorXY, null, null, "Vector X,Y", "The Vector for the Scroll Texture");

                            Moristyles.PartingLine();
                            
                            ShaderPropertyOptimizer(_NoiseTexture, _NoiseTextureUVSwitch, null, "Noise Texture", "The Noise Texture for the Emission Scroll system");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_NoiseTexture);
                            }

                            ShaderPropertyOptimizer(_NoiseSpeed, null, null, "Noise speed", "Controls the scrolling speed of the Noise Texture");
                            ShaderPropertyOptimizer(_NoiseVectorXY, null, null, "Noise Vector X,Y", "The Vector for the Noise Texture");

                            Moristyles.PartingLine();
                            
                            ShaderPropertyOptimizer(_EmissionScrollMask, _EmissionScrollMaskUVSwitch, _EmissionScrollMaskTexChannelSwitch, "Mask", "Mask the Parts that you dont want to get affected by the Emission Scroll Texture\nFirst Channel for ESv1\nSecond Channel does not apply to ESv1 !");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_EmissionScrollMask);
                            }
                        }
                        else if (_EmissionScrollToggle.floatValue == 2) // V2
                        {
                            if (_AudioLinkSwitch.floatValue > 0) //AudioLInk Tooltip
                                materialEditor.ShaderProperty(_AudioLinkTooltip, new GUIContent("Info: Some Properties from ESV2 wont work for AudioLink\nProperties for AudioLink:\nTint\nScroll speed\nScroll Offset\nCoordinates X,Y\nMask Red Channel = Bands\nMask Green Channel = Waveform", ""));
                            ShaderPropertyOptimizer(_IgnoreNormalsESv2, null, null, "Ignore Normal Maps", "Forces the Emission Scroll to Ignore Normal Maps for a smooth outcome");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                ShaderPropertyOptimizer(_ESRenderMethod, null, null, "Render Method", "Controls the Rendermethod the Lines are rendered on Screen");
                            }
                            
                            if (_ESRenderMethod.floatValue == 3)
                            {
                                material.SetInt("_ESRenderMethodAnimated", 1); // this property causes Voronoi to break in non Animated state so there is none for it.
                                ShaderPropertyOptimizer(_ESVoronoiScale, null, null, "Voronoi Scale", "Controls the Size of the lines");
                                ShaderPropertyOptimizer(_ESVoronoiSpeed, null, null, "Voronoi Speed", "Controls the Speed of the lines");
                            }

                            ShaderPropertyOptimizer(_AudioLinkSwitch, null, null, "AudioLink Mode", "Uses a Global _AudioTexture which can be provided by a World to synchronise with your Mesh");
                            if (_AudioLinkSwitch.floatValue > 0) //AudioLink General
                            {
                                ShaderPropertyOptimizer(_AudioHueSpeed, null, null, "Audio Hue Speed", "Controls the speed of the Hue shift Operation");
                                ShaderPropertyOptimizer(_AudioLinkColor, null, null, "Audio Color", "Controls the Color of the AudioLink effect");
                            }

                            if (_AudioLinkSwitch.floatValue == 1 || _AudioLinkSwitch.floatValue == 3) // AudioLink Bands
                            {
                                ShaderPropertyOptimizer(_AudioLinkBandHistory, null, null, "Audio Band History", "Since AudioLink v2 has a max History of 128 texels but v1 just 32 you could just use 32 here(only controls v2) and have v1 and v2 being seemingly equal, otherwise you can just go up to 128 for Worlds that support v2 instead of v1");
                                ShaderPropertyOptimizer(_AudioBandIntensity, null, null, "Audio Band Intensity 0,1,2,3", "gives you control over each Band to set its Intensity starting from low(Bass) to higher frequencies. The way the _AudioTexture is used here is by laying each channel on top of each other instead of sampling each channel separately next to each other");
                            }

                            if (_AudioLinkSwitch.floatValue == 2 || _AudioLinkSwitch.floatValue == 3) //AudioLink Waveform
                            {
                                ShaderPropertyOptimizer(_AudioLinkWaveformMirrorToggle, null, null, "Audio Waveform Mode", "Mirrors the Waveform");
                                if (_AudioLinkWaveformMirrorToggle.floatValue < 2)
                                    ShaderPropertyOptimizer(_WaveformThickness, null, null, "Waveform Thickness", "thickens or makes the Waveform thinner");
                                ShaderPropertyOptimizer(_WaveformRotation, null, null, "Waveform Rotation", "Rotates the Waveform");
                                ShaderPropertyOptimizer(_WaveformUVShift, null, null, "Waveform UV Shift", "Shifts the Audiolink Waveform to the UVs .x Axis\nThis is needed if the UV has shifted for use of the Inventory System");
                                ShaderPropertyOptimizer(_WaveformCoordinates, null, null, "Waveform Tiling Offset", ".xy = Tiling\n.zw = Offset");
                            }

                            if (_AudioLinkSwitch.floatValue > 0)
                                Moristyles.PartingLine();
                            ShaderPropertyOptimizer(_EmissionscrollTint, null, null, "Diffuse Tint", "Controls the tint to Maintex of the Emissionscroll V2 lines");
                            ShaderPropertyOptimizer(_EmissionscrollColor, null, null, "Color", "Controls the Color of the Emissionscroll V2 lines\nNOTE that this property does not affect AudioLink and will solely drive the Emissionscroll effect");
                            ShaderPropertyOptimizer(_ESSize, null, null, "Size", "Controls the size of the lines\nNOTE that this property does not affect AudioLink and will solely drive the Emissionscroll effect");
                            ShaderPropertyOptimizer(_ESSharpness, null, null, "Sharpness", "Controls the sharpness of the lines\nNOTE that this property does not affect AudioLink and will solely drive the Emissionscroll effect");
                            ShaderPropertyOptimizer(_ESLevelOffset, null, null, "Level Offset", "Offsets the Lines slightly\nThis property should theoretically not be lowered below 0 but its left in for if you want to have a pulse only effect when set to -1");
                            ShaderPropertyOptimizer(_ESSpeed, null, null, "Scroll speed", "Controls the scroll speed of the Emission Scroll lines");
                            ShaderPropertyOptimizer(_ESScrollOffset, null, null, "Scroll Offset", "Offsets the Lines");
                            ShaderPropertyOptimizer(_ESCoordinates, null, null, "Coordinates X,Y", "Controls the coordinates of the scrolling lines");
                            ShaderPropertyOptimizer(_EmissionScrollMask, _EmissionScrollMaskUVSwitch, _EmissionScrollMaskTexChannelSwitch, "Mask", "Mask the Parts that you dont want to get affected by the Emission Scroll Texture\nFirst Channel for ESv2 + AudioLink Bands\nSecond Channel for AudioLink Waveform");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_EmissionScrollMask);
                            }
                        }
                    }

                    showLTCGI = Moristyles.ShurikenFoldout("LTCGI", showLTCGI, Moristyles.shurikenIndent);
                    if (showLTCGI)
                    {
                        #if LTCGI_INSTALLED //derived from Package Assembly definition
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.ShaderProperty(_LTCGI, new GUIContent("Toggle LTCGI", "LTCGI On, Off\nThis is a Shader Keyword and can therefore not be animated"));
                                if (_LTCGI.floatValue == 1)
                                {
                                    ShaderPropertyOptimizer(_ToggleLTCGIDiffuse, null, null, "Toggle LTCGI Diffuse", "Defines if your Material receives diffuse Lighting from the LTCGI Lightsource");
                                    ShaderPropertyOptimizer(_ToggleLTCGISpecular, null, null, "Toggle LTCGI Specular", "Specular = Reflections\nLTCGI Reflections will behave like BRDF Cubemap reflections which means that they will be dependent on what you set in the Reflections->Cubemap section");
                                }
                            }
                        #else
                            material.SetInt("_LTCGI", 0);
                            material.DisableKeyword("LTCGI");
                            materialEditor.ShaderProperty(_LTCGInotInstalledTooltip, new GUIContent("YOU DO NOT HAVE THE LTCGI PACKAGE INSTALLED\nINSTALL LTCGI FROM PI'S GITHUB PAGE: https://github.com/PiMaker/ltcgi", ""));
                        #endif
                    }

                    showFlipbook = Moristyles.ShurikenFoldout("Flipbook and Decals", showFlipbook, Moristyles.shurikenIndent);
                    if (showFlipbook)
                    {
                        ShaderPropertyOptimizer(_FlipbookToggle, null, null, "Toggle Flipbook", "Flipbook On, Off");
                        if (_FlipbookToggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_Flipbook, _FlipbookColor, _FlipbookUVSwitch, "Flipbook", "The Texture for the Flipbook. Sprite Sheets are normally used for that");
                            ShaderPropertyOptimizer(_FlipbookMode, null, null, "Mode", "Sets the Mode for the Flipbook and controls how to interact with the Main Texture"); //Remove _ToggleEmissiveFlipbook in later versions when the property has been adapted for most users and then also change it inside the Base code
                            ShaderPropertyOptimizer(_FlipbookTint, null, null, "Tint to Diffuse", "Multiplies the Flipbook with the Main Texture");
                            ShaderPropertyOptimizer(_Columns, null, null, "Columns", "The Amount of Columns");
                            ShaderPropertyOptimizer(_Rows, null, null, "Rows", "The Amount of Rows");
                            ShaderPropertyOptimizer(_MaxFrames, null, null, "Max Frames", "The Amount of Maximum Frames in the Sprite Sheet");
                            ShaderPropertyOptimizer(_Speed, null, null, "Speed", "Defines the speed that the Flipbook is playing as");
                            ShaderPropertyOptimizer(_RotateFlipbook, null, null, "Rotate", "Rotates the Flipbook");
                            ShaderPropertyOptimizer(_FlipbookUVShift, null, null, "Flipbook UV Shift", "Shifts the Flipbook to the UVs .x Axis\nThis is needed if the UV has shifted for use of the Inventory System");
                            ShaderPropertyOptimizer(_FlipbookAudioLinkToggle, null, null, "Use ESv2 AudioLink", "You can reuse the AudioLink effect from Emission Scroll v2 on Flipbooks\nToggling this and deactivating Emission Scroll v2 will make it so that the AudioLink Effect is only visible on this feature here");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                            {
                                materialEditor.TextureScaleOffsetProperty(_Flipbook);
                            }
                        }
                        
                        Moristyles.PartingLine();
                        
                        ShaderPropertyOptimizer(_DecalToggle, null, null, "Toggle Decals", "Decal On, Off");
                        if (_DecalToggle.floatValue == 1)
                        {
                            ShaderPropertyOptimizer(_Decal1, _Decal1Color, _Decal1UVSwitch, "Decal 1", "The Texture for the Decal");
                            if (_Decal1.textureValue != null)
                            {
                                ShaderPropertyOptimizer(_Decal1WrapMode, null, null, "Wrap Mode", "Defines the Wrap Mode of the Decal");
                                ShaderPropertyOptimizer(_Decal1Mode, null, null, "Mode", "Sets the Mode for the Decal and controls how to interact with the Main Texture");
                                ShaderPropertyOptimizer(_Decal1Tint, null, null, "Tint to Diffuse", "Multiplies the Decal with the Main Texture");
                                ShaderPropertyOptimizer(_RotateDecal1, null, null, "Rotate", "Rotates the Decal");
                                ShaderPropertyOptimizer(_Decal1AudioLinkToggle, null, null, "Use ESv2 AudioLink", "You can reuse the AudioLink effect from Emission Scroll v2 on This Decal here\nToggling this and deactivating Emission Scroll v2 will make it so that the AudioLink Effect is only visible on this feature here");
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                    materialEditor.TextureScaleOffsetProperty(_Decal1);
                            }
                            Moristyles.PartingLine();
                            ShaderPropertyOptimizer(_Decal2, _Decal2Color, _Decal2UVSwitch, "Decal 2", "The Texture for the Decal");
                            if (_Decal2.textureValue != null)
                            {
                                ShaderPropertyOptimizer(_Decal2WrapMode, null, null, "Wrap Mode", "Defines the Wrap Mode of the Decal");
                                ShaderPropertyOptimizer(_Decal2Mode, null, null, "Mode", "Sets the Mode for the Decal and controls how to interact with the Main Texture");
                                ShaderPropertyOptimizer(_Decal2Tint, null, null, "Tint to Diffuse", "Multiplies the Decal with the Main Texture");
                                ShaderPropertyOptimizer(_RotateDecal2, null, null, "Rotate", "Rotates the Decal");
                                ShaderPropertyOptimizer(_Decal2AudioLinkToggle, null, null, "Use ESv2 AudioLink", "You can reuse the AudioLink effect from Emission Scroll v2 on This Decal here\nToggling this and deactivating Emission Scroll v2 will make it so that the AudioLink Effect is only visible on this feature here");
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                    materialEditor.TextureScaleOffsetProperty(_Decal2);
                            }
                            Moristyles.PartingLine();
                            ShaderPropertyOptimizer(_Decal3, _Decal3Color, _Decal3UVSwitch, "Decal 3", "The Texture for the Decal");
                            if (_Decal3.textureValue != null)
                            {
                                ShaderPropertyOptimizer(_Decal3WrapMode, null, null, "Wrap Mode", "Defines the Wrap Mode of the Decal");
                                ShaderPropertyOptimizer(_Decal3Mode, null, null, "Mode", "Sets the Mode for the Decal and controls how to interact with the Main Texture");
                                ShaderPropertyOptimizer(_Decal3Tint, null, null, "Tint to Diffuse", "Multiplies the Decal with the Main Texture");
                                ShaderPropertyOptimizer(_RotateDecal3, null, null, "Rotate", "Rotates the Decal");
                                ShaderPropertyOptimizer(_Decal3AudioLinkToggle, null, null, "Use ESv2 AudioLink", "You can reuse the AudioLink effect from Emission Scroll v2 on This Decal here\nToggling this and deactivating Emission Scroll v2 will make it so that the AudioLink Effect is only visible on this feature here");
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                    materialEditor.TextureScaleOffsetProperty(_Decal3);
                            }
                            Moristyles.PartingLine();
                            ShaderPropertyOptimizer(_Decal4, _Decal4Color, _Decal4UVSwitch, "Decal 4", "The Texture for the Decal");
                            if (_Decal4.textureValue != null)
                            {
                                ShaderPropertyOptimizer(_Decal4WrapMode, null, null, "Wrap Mode", "Defines the Wrap Mode of the Decal");
                                ShaderPropertyOptimizer(_Decal4Mode, null, null, "Mode", "Sets the Mode for the Decal and controls how to interact with the Main Texture");
                                ShaderPropertyOptimizer(_Decal4Tint, null, null, "Tint to Diffuse", "Multiplies the Decal with the Main Texture");
                                ShaderPropertyOptimizer(_RotateDecal4, null, null, "Rotate", "Rotates the Decal");
                                ShaderPropertyOptimizer(_Decal4AudioLinkToggle, null, null, "Use ESv2 AudioLink", "You can reuse the AudioLink effect from Emission Scroll v2 on This Decal here\nToggling this and deactivating Emission Scroll v2 will make it so that the AudioLink Effect is only visible on this feature here");
                                using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                    materialEditor.TextureScaleOffsetProperty(_Decal4);
                            }
                            Moristyles.PartingLine();
                            ShaderPropertyOptimizer(_DecalUVShift, null, null, "Decals UV Shift", "Shifts the Decal to the UVs .x Axis\nThis is needed if the UV has shifted for use of the Inventory System");
                            ShaderPropertyOptimizer(_DecalMask, _DecalMaskUVSwitch, null, "Decal Mask", "Masks the Decals\nChannel R = Decal 1\nChannel G = Decal 2\nChannel B = Decal 3\nChannel A = Decal 4");
                            using (new EditorGUI.DisabledScope(_ShaderOptimizerEnabled.floatValue == 1))
                                materialEditor.TextureScaleOffsetProperty(_DecalMask);
                        }
                        Moristyles.PartingLine();
                    }
                }
            }

            // Rendering Options
            showRenderingOptions = Moristyles.ShurikenFoldout("Rendering Options", showRenderingOptions, 0);
            if (showRenderingOptions)
            {
                Moristyles.ShurikenHeaderCentered("Blend Options");
                materialEditor.ShaderProperty(_SrcBlend, new GUIContent("Source Blend RGB", ""));
                materialEditor.ShaderProperty(_DstBlend, new GUIContent("Destination Blend RGB", ""));
                materialEditor.ShaderProperty(_BlendOpRGB, new GUIContent("Blend Op RGB", ""));

                Moristyles.PartingLine();

                if (_AdvancedExperimentalToggle.floatValue != 0)
                {
                    materialEditor.ShaderProperty(_SourceBlendAlpha, new GUIContent("Source Blend Alpha", ""));
                    materialEditor.ShaderProperty(_DestinationBlendAlpha, new GUIContent("Destination Blend Alpha", ""));
                    materialEditor.ShaderProperty(_BlendOpAlpha, new GUIContent("Blend Op Alpha", ""));

                    Moristyles.PartingLine();
                }
                
                //materialEditor.ShaderProperty(_MaskClipValue, new GUIContent("Mask Clip Value", "")); //Not used anymore
                materialEditor.ShaderProperty(_AlphatoCoverage, new GUIContent("Alpha to Coverage", ""));
                if (_ShaderOptimizerEnabled.floatValue == 1)
                    EditorGUI.BeginDisabledGroup(true);
                {
                    materialEditor.ShaderProperty(_IgnoreProjector, new GUIContent("Ignore Projector", "This value is automatically set to true if the Inventory System is toggled on or the shader is in Transparent or Cutout Mode and will be activated after Shader locking\nThis Setting requires the Shader Optimizer to run on the material aka. locking in the material in optimized Shader"));
                    materialEditor.ShaderProperty(_ForceNoShadowCasting, new GUIContent("Force no Shadow Casting", "This will force the shader to exclude any Shadow Caster Pass, even from the Fallback Shader\nThis also comes in handy when you want to (Disable Lightmode = ShadowCaster) which will not remove the Shadow Caster Pass due to the set Fallback Shader\nThis Setting requires the Shader Optimizer to run on the material aka. locking in the material in optimized Shader"));
                    materialEditor.ShaderProperty(_LightModes, new GUIContent("Disable LightModes", "Here you can siable individual Shader Passes\nKeep in mind that ShadowCaster Pass needs (Force no Shadow Casting) to be enabled as well due to the Fallback Shader\nThe Shader will convert Add Pass Lights to Vertex Lights if the ForwardAdd Pass is selected to be disabled"));
                }
                EditorGUI.EndDisabledGroup();

                if (_AdvancedExperimentalToggle.floatValue != 0) materialEditor.ShaderProperty(_ColorMask, new GUIContent("Color Mask", ""));

                Moristyles.PartingLine();

                if (_AdvancedExperimentalToggle.floatValue != 0)
                {
                    Moristyles.ShurikenHeaderCentered("Stencil Options");
                    materialEditor.ShaderProperty(_StencilBufferReference, new GUIContent("Stencil Buffer Reference", ""));
                    materialEditor.ShaderProperty(_StencilBufferReadMask, new GUIContent("Stencil Buffer Read Mask", ""));
                    materialEditor.ShaderProperty(_StencilBufferWriteMask, new GUIContent("Stencil Buffer Write Mask", ""));
                    materialEditor.ShaderProperty(_StencilBufferComparison, new GUIContent("Stencil Buffer Comparison", ""));
                    materialEditor.ShaderProperty(_StencilBufferPassFront, new GUIContent("Stencil Buffer Pass Front", ""));
                    materialEditor.ShaderProperty(_StencilBufferFailFront, new GUIContent("Stencil Buffer Fail Front", ""));
                    materialEditor.ShaderProperty(_StencilBufferZFailFront, new GUIContent("Stencil Buffer ZFail Front", ""));

                    Moristyles.PartingLine();
                }

                Moristyles.ShurikenHeaderCentered("Depth Options");
                materialEditor.ShaderProperty(_ZWrite, new GUIContent("ZWrite Mode", ""));
                materialEditor.ShaderProperty(_ZTestMode, new GUIContent("ZTest Mode", ""));
                if (_AdvancedExperimentalToggle.floatValue != 0)
                {
                    materialEditor.ShaderProperty(_DepthOffsetFactor, new GUIContent("Depth Offset Factor", ""));
                    materialEditor.ShaderProperty(_DepthOffsetUnits, new GUIContent("Depth Offset Units", ""));
                }
                Moristyles.PartingLine();
            }

            materialEditor.RenderQueueField();
            Moristyles.DrawButtons();


            //Draw new ShaderProperties with animateable Buttons
            void ShaderPropertyOptimizer(MaterialProperty firstProperty, MaterialProperty secondProperty, MaterialProperty thirdProperty, string label, string toolTip)
            {
                var style = new GUIStyle(GUI.skin.button);
                GUILayout.BeginHorizontal();
                //Setup automated Animated Property assignment, if first property is not a texture and second property is null then use first property
                //If second property does exist then use it instead. This extra check only handles TexturePropertySingleLine

                //We need to declare a _Dummy property in the Shader that is always 0 to get this to work
                string animatedProperty = "_Dummy";                                                                              //animatedProperty = secondProperty
                string animatedProperty2 = "_Dummy";                                                                             //animatedProperty2 = thirdProperty
                if (secondProperty != null) animatedProperty = secondProperty.name + "Animated";                                 //handle second property
                if (thirdProperty != null) animatedProperty2 = thirdProperty.name + "Animated";                                  //handle third property
                if (firstProperty.type != MaterialProperty.PropType.Texture) animatedProperty = firstProperty.name + "Animated"; //First prop is no texture, textures do not have animated states
                int animatedPropertiesEqual0 = material.GetInt(animatedProperty) + material.GetInt(animatedProperty2);
                using (new EditorGUI.DisabledScope(animatedPropertiesEqual0 == 0 && _ShaderOptimizerEnabled.floatValue == 1))
                {
                    //sets if its a single line property or not depending if one property is used or more
                    //Unity only provides one property in materialEditor.ShaderProperty hence why everything else we do is a TexturePropertySingleLine
                    if (firstProperty.type != MaterialProperty.PropType.Texture && secondProperty == null && thirdProperty == null)
                    {
                        materialEditor.ShaderProperty(firstProperty, new GUIContent(label, toolTip));
                    }
                    else if (firstProperty.type == MaterialProperty.PropType.Texture && secondProperty == null && thirdProperty == null)
                    {
                        materialEditor.TexturePropertySingleLine(new GUIContent(label, toolTip), firstProperty);
                    }
                    else
                    {
                        materialEditor.TexturePropertySingleLine(new GUIContent(label, toolTip), firstProperty, secondProperty, thirdProperty);
                    }

                    //Drawing clickable Button to toggle Animated state for given property
                    //Sets the checkmark based on if the animated property is set or not
                    if (material.GetInt(animatedProperty) == 0)
                    {
                        style.normal.textColor = Color.black;
                        style.normal.background = Texture2D.redTexture;
                    }
                    else
                    {
                        style.normal.textColor = Color.red;
                        style.normal.background = Texture2D.whiteTexture;
                    }

                    //Draw Button
                    if (GUILayout.Button(new GUIContent("﹡", "Toggle: " + animatedProperty), style, GUILayout.Width(20)))
                    {
                        int getFirstSelectedPropertyValue = material.GetInt(animatedProperty);
                        foreach (Object o in materialEditor.targets)
                        {
                            var m = (Material) o;
                            m.SetInt(animatedProperty, getFirstSelectedPropertyValue == 0 ? 1 : 0);
                        }
                    }

                    if (thirdProperty != null) //handle third property if exists
                    {
                        //Sets the checkmark based on if the animated property is set or not
                        if (material.GetInt(animatedProperty2) == 0)
                        {
                            style.normal.textColor = Color.black;
                            style.normal.background = Texture2D.redTexture;
                        }
                        else
                        {
                            style.normal.textColor = Color.red;
                            style.normal.background = Texture2D.whiteTexture;
                        }
                    
                        //Draw Button
                        if (GUILayout.Button(new GUIContent("﹡", "Toggle: " + animatedProperty2), style, GUILayout.Width(20)))
                        {
                            int getFirstSelectedPropertyValue = material.GetInt(animatedProperty2);
                            foreach (Object o in materialEditor.targets)
                            {
                                var m = (Material) o;
                                m.SetInt(animatedProperty2, getFirstSelectedPropertyValue == 0 ? 1 : 0);
                            }
                        }
                    } 
                    GUILayout.EndHorizontal();
                }
            }
            
            
            //Setup Inventory renaming and property drawer
            void SetupInventoryProperty(MaterialProperty prop, string TagName, string ItemNumber)
            {
                string newInventoryItemName = material.GetTag(TagName, false);
                string defaultTag = "Item " + ItemNumber + ": ";
                string newInventoryItemNamePlusTag = defaultTag + newInventoryItemName;

                if (_InventoryRenameProperties.floatValue == 1)
                {
                    GUILayout.BeginHorizontal();
                    newInventoryItemName = EditorGUILayout.TextField(newInventoryItemNamePlusTag, newInventoryItemName);
                    GUILayout.EndHorizontal();
                    foreach (Object o in materialEditor.targets)
                    {
                        var m = (Material) o;
                        m.SetOverrideTag(TagName, newInventoryItemName);
                    }
                }
                else
                    materialEditor.ShaderProperty(prop, new GUIContent(newInventoryItemNamePlusTag, "Item to Dissolve/Materialize:\n" + newInventoryItemName));
            }
        }

        private static void ShaderOptimizerButton(MaterialProperty shaderOptimizer, MaterialEditor materialEditor)
        {
            EditorGUI.BeginChangeCheck();
            GUILayout.Button(shaderOptimizer.floatValue == 0 ? "Lock in optimized Shader" : "Unlock from optimized Shader");
            if (EditorGUI.EndChangeCheck())
            {
                shaderOptimizer.floatValue = shaderOptimizer.floatValue == 1 ? 0 : 1;
                if (shaderOptimizer.floatValue == 1)
                    foreach (Material m in materialEditor.targets)
                    {
                        var props = MaterialEditor.GetMaterialProperties(new UnityEngine.Object[] {m});
                        if (!ShaderOptimizer.Lock(m, props))
                            m.SetFloat(shaderOptimizer.name, 0);
                    }
                else
                    foreach (Material m in materialEditor.targets)
                        if (!ShaderOptimizer.Unlock(m))
                            m.SetFloat(shaderOptimizer.name, 1);
            }
            EditorGUILayout.Space(4);
        }
    }
}