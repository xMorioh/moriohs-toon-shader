﻿#region

using System;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using UnityEditor;
using UnityEngine;
#endregion

// help link https://docs.unity3d.com/ScriptReference/EditorStyles.html
// ---DISCLAIMER--- THIS CODE IS BASED OFF OF "SYNQARK"'s ARKTOON-SHADERS AND "XIEXE"'s UNITY-SHADERS. FOR MORE INFORMATION PLEASE REFER TO THE ORIGINAL BASE WRITER "https://github.com/synqark", "https://github.com/synqark/Arktoon-Shaders" or "https://github.com/Xiexe", "https://github.com/Xiexe/Xiexes-Unity-Shaders"

namespace Morioh.Moriohs_Toon_Shader.Editor
{
    //public struct RemoteVersion //default way of calling the remote version
    //{
    //    public string version { get;set;}
    //}
    
    internal class Moristyles : MonoBehaviour
    {
        //Verion Check
        //Local Version
        private static readonly string localVersion = "2.0.7"; //we could read the package.json in Packages file for the local version but the path is different on my development OS, so we need to edit this manually here too
        
        //Remote Version with Session Caching, thank you Razgriz#8110, we need to Cache the value here if the Website is using must-revalidate in HTML Header
        //because otherwise the Inspector will hang 1-2 seconds on the Webrequest on every Inspector invocation, Gitlab currently does not do that but may in the future
        //By declaring a new Cache in between we also go around the need of using PooledConnectionLifetime and save on client request sockets
        private class RemoteVersionContainer
        {
            public string version;
        }

        internal static string RemoteVersion
        {
            get
            {
                string version = UnityEditor.SessionState.GetString("RemoteVersion", "");
                if(String.IsNullOrEmpty(version))
                {
                    try //Catchblock if the Remote Server is down or the User has no Internet or the file does not exist anymore on the remote server etc.
                    {
                        //Thank you orels1#2775 for this Webrequest approach
                        string packageJson = new HttpClient().GetAsync("https://gitlab.com/xMorioh/moriohs-toon-shader/-/raw/master/package.json").Result.Content.ReadAsStringAsync().Result;
                        RemoteVersionContainer remote = JsonUtility.FromJson<RemoteVersionContainer>(packageJson);
                        UnityEditor.SessionState.SetString("RemoteVersion", remote.version);
                    }
                    catch
                    {
                        //We are not populating the SessionState key here because we want a immediate remote request as soon as connection is up again
                        //This may be more expensive, i do not know by how much
                        version = "unable to contact remote server";
                    }
                }
                return version;
            }
        }
        //private static readonly RemoteVersion remote = JsonConvert.DeserializeObject<RemoteVersion>(new HttpClient().GetAsync("https://gitlab.com/xMorioh/moriohs-toon-shader/-/raw/master/package.json").Result.Content.ReadAsStringAsync().Result); //default way of calling the remote version, does not include checks for no internet access/Catchblock nor SessionState caching
        public static readonly string ver = "<color=#00ffffff> ✿ {  </color>Moriohs Toon Shader v." + "<color=#FFFF00ff>" + localVersion + "</color> | <color=#00ff00ff>Git-Rev.2</color>" + "<color=#00ffffff>  } ✿ </color>";
        
        

        //Shuriken Toggle and variants of it
        private static Rect DrawShuriken(string title, Vector2 contentOffset, int HeaderHeight)
        {
            var style = new GUIStyle("ShurikenModuleTitle")
            {
                font = new GUIStyle(EditorStyles.boldLabel).font,
                border = new RectOffset(15, 7, 4, 4),
                fixedHeight = HeaderHeight,
                contentOffset = contentOffset
            };
            var rect = GUILayoutUtility.GetRect(16f, HeaderHeight, style);
            GUI.Box(rect, title, style);
            return rect;
        }

        public const int shurikenIndent = 11;
        public static bool ShurikenFoldout(string title, bool display, int indent)
        {
            var rect = DrawShuriken(title, new Vector2(20+indent, -2f), 22);
            var e = Event.current;
            var toggleRect = new Rect(rect.x + 4+indent, rect.y + 2f, 13f, 13f);
            if (e.type == EventType.Repaint) EditorStyles.foldout.Draw(toggleRect, false, false, display, false);
            if (e.type == EventType.MouseDown && rect.Contains(e.mousePosition))
            {
                display = !display;
                e.Use();
            }
            return display;
        }
        //Shuriken Toggle and variants of it end


        //Title Shuriken for text only
        private static void DrawShurikenCenteredTitle(string title, Vector2 contentOffset, int HeaderHeight)
        {
            var style = new GUIStyle("ShurikenModuleTitle")
            {
                font = new GUIStyle(EditorStyles.boldLabel).font,
                fontSize = 12,
                border = new RectOffset(15, 7, 4, 4),
                fixedHeight = HeaderHeight,
                contentOffset = contentOffset,
                alignment = TextAnchor.MiddleCenter
            };
            var rect = GUILayoutUtility.GetRect(16f, HeaderHeight, style);

            GUI.Box(rect, title, style);
        }
    
        public static void ShurikenHeaderCentered(string title)
        {
            DrawShurikenCenteredTitle(title, new Vector2(0f, -2f), 28);
        }
        //Title Shuriken for text only end
    
    
        //parting lines
        private static GUIStyle _LineStyle;
        private static GUIStyle LineStyle => _LineStyle ?? (_LineStyle = new GUIStyle
        {
            normal =
            {
                background = EditorGUIUtility.whiteTexture
            },
            stretchWidth = true
        });
    
        private static void GUILine(Color color, float height = 0f)
        {
            Rect position = GUILayoutUtility.GetRect(0f, float.MaxValue, height, height, LineStyle);

            if (Event.current.type == EventType.Repaint)
            {
                Color orgColor = GUI.color;
                GUI.color = orgColor * color;
                LineStyle.Draw(position, false, false, false, false);
                GUI.color = orgColor;
            }
        }
    
        public static void PartingLine()
        {
            GUILayout.Space(5);
            GUILine(new Color(.7f, .7f, .7f), 1.5f);
            GUILayout.Space(5);
        }
        //parting lines end

        //exrta buttons
        private static void gitlabversioncheckbutton(int Width, int Height)
        {
            if (GUILayout.Button("Check Version", GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader");
        }

        private static void gitupdatebutton(int Width, int Height)
        {
            string updateStatus = "You are up to date!";
            if (RemoteVersion == "unable to contact remote server")
                updateStatus = "Fetching Updates...";
            if (RemoteVersion != localVersion && RemoteVersion != "unable to contact remote server")
            {
                updateStatus = "Update to v." + RemoteVersion;
                #if PACKAGE_MORIOHSTOONSHADER
                    if (GUILayout.Button(updateStatus, GUILayout.Width(Width), GUILayout.Height(Height))) UnityEditor.PackageManager.Client.Add("https://gitlab.com/xMorioh/moriohs-toon-shader.git");
                #else
                    if (GUILayout.Button(updateStatus, GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader/-/archive/master/moriohs-toon-shader-master.zip");
                #endif
            }
            else
                #pragma warning disable CS0642
                if (GUILayout.Button(updateStatus, GUILayout.Width(Width), GUILayout.Height(Height)));
        }

        private static void manualbutton(int Width, int Height)
        {
            if (GUILayout.Button("Show Manual", GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader/-/wikis/Mori's-Toon-Shader-Manual");
        }

        public static void DrawButtons()
        {
            PartingLine();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            //gitlabversioncheckbutton(125, 30);
            gitupdatebutton(135, 30);
            manualbutton(135, 30);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        //exrta buttons end

        public class moriohVec3FloatDrawer : MaterialPropertyDrawer
        {
            // Draw vector4 as vector3 and float
            // [moriohVec3Float]
            //Ported from Liltoon
            public override void OnGUI(Rect position, MaterialProperty prop, GUIContent label, MaterialEditor editor)
            {
                var labels = label.text.Split('|');
                GUIContent constructedLabel = new GUIContent(labels[0], label.tooltip);

                var vec = new Vector3(prop.vectorValue.x, prop.vectorValue.y, prop.vectorValue.z);
                //float length = prop.vectorValue.w;
                EditorGUIUtility.wideMode = true;

                EditorGUI.BeginChangeCheck();
                EditorGUI.showMixedValue = prop.hasMixedValue;
                vec = EditorGUI.Vector3Field(position, constructedLabel, vec);
                //length = EditorGUI.FloatField(EditorGUILayout.GetControlRect(), labels[1], length);
                EditorGUI.showMixedValue = false;

                if(EditorGUI.EndChangeCheck())
                {
                    //prop.vectorValue = new Vector4(vec.x, vec.y, vec.z, length); //draw "length" as extra float prop below vec3, needs uncommenting above as well
                    prop.vectorValue = new Vector3(vec.x, vec.y, vec.z);
                }
            }
        }
        
        public class moriohVec2FloatDrawer : MaterialPropertyDrawer
        {
            // Draw vector4 as vector2 and float
            // [moriohVec2Float]
            //Ported from Liltoon
            public override void OnGUI(Rect position, MaterialProperty prop, GUIContent label, MaterialEditor editor)
            {
                var labels = label.text.Split('|');
                GUIContent constructedLabel = new GUIContent(labels[0], label.tooltip);
                
                var vec = new Vector2(prop.vectorValue.x, prop.vectorValue.y);
                //float length = prop.vectorValue.w;

                EditorGUIUtility.wideMode = true;

                EditorGUI.BeginChangeCheck();
                EditorGUI.showMixedValue = prop.hasMixedValue;
                vec = EditorGUI.Vector2Field(position, constructedLabel, vec);
                //length = EditorGUI.FloatField(EditorGUILayout.GetControlRect(), labels[1], length);
                EditorGUI.showMixedValue = false;

                if(EditorGUI.EndChangeCheck())
                {
                    //prop.vectorValue = new Vector4(vec.x, vec.y, length); //draw "length" as extra float prop below vec3, needs uncommenting above as well
                    prop.vectorValue = new Vector3(vec.x, vec.y);
                }
            }
        }
        
        // Simple auto-laid out information box, uses materialproperty display name as text | Ported from Kaj's Editor
        public class HelpBoxDrawer : MaterialPropertyDrawer
        {
            private readonly MessageType type;

            public HelpBoxDrawer()
            {
                type = MessageType.Info;
            }

            public HelpBoxDrawer(float f)
            {
                type = (MessageType) (int) f;
            }

            public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor editor)
            {
                EditorGUILayout.HelpBox(label, type);
            }

            public override float GetPropertyHeight(MaterialProperty prop, string label, MaterialEditor editor)
            {
                return -4f; // Remove the extra drawer padding + helpbox extra padding
            }
        }

        // Disabled lightmodes mask drawer. | Ported from Kaj's Editor
        public class DisabledLightModesDrawer : MaterialPropertyDrawer
        {
            readonly string[] enumNames;
    
            public DisabledLightModesDrawer()
            {
                enumNames = Enum.GetNames(typeof(LightMode));
            }
    
            public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor materialEditor)
            {
                EditorGUI.showMixedValue = prop.hasMixedValue;
                EditorGUIUtility.labelWidth = 0f;
                EditorGUI.BeginChangeCheck();
                int lightModesMask = EditorGUILayout.MaskField(label, (int)prop.floatValue, enumNames);
                if (EditorGUI.EndChangeCheck())
                {
                    prop.floatValue = lightModesMask;
                    SetMaterialsLightMode(materialEditor, lightModesMask);
                }
            }
            
            private void SetMaterialsLightMode(MaterialEditor materialEditor, int lightModesMask)
            {
                if ((lightModesMask & (int)LightMode.Always) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "Always", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "Always", true);
                if ((lightModesMask & (int)LightMode.ForwardBase) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "ForwardBase", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "ForwardBase", true);
                if ((lightModesMask & (int)LightMode.ForwardAdd) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "ForwardAdd", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "ForwardAdd", true);
                if ((lightModesMask & (int)LightMode.Deferred) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "Deferred", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "Deferred", true);
                if ((lightModesMask & (int)LightMode.ShadowCaster) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "ShadowCaster", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "ShadowCaster", true);
                if ((lightModesMask & (int)LightMode.MotionVectors) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "MotionVectors", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "MotionVectors", true);
                if ((lightModesMask & (int)LightMode.PrepassBase) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "PrepassBase", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "PrepassBase", true);
                if ((lightModesMask & (int)LightMode.PrepassFinal) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "PrepassFinal", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "PrepassFinal", true);
                if ((lightModesMask & (int)LightMode.Vertex) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "Vertex", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "Vertex", true);
                if ((lightModesMask & (int)LightMode.VertexLMRGBM) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "VertexLMRGBM", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "VertexLMRGBM", true);
                if ((lightModesMask & (int)LightMode.VertexLM) != 0)
                    SetMaterialsLightModeEnabled(materialEditor.targets, "VertexLM", false);
                else SetMaterialsLightModeEnabled(materialEditor.targets, "VertexLM", true);
            }
            
            private void SetMaterialsLightModeEnabled(UnityEngine.Object[] mats, string pass, bool enabled)
            {
                foreach (Material m in mats)
                    m.SetShaderPassEnabled(pass, enabled);
            }
        
            public override float GetPropertyHeight(MaterialProperty prop, string label, MaterialEditor editor)
            {
                return -2;
            }
        }
        
        // Enum with normal editor width, rather than MaterialEditor Default GUI widths
        // Would be nice if Decorators could access Drawers too so this wouldn't be necessary for something to trivial
        // Adapted from Unity interal MaterialEnumDrawer https://github.com/Unity-Technologies/UnityCsReference/
        // Ported from Kaj's Editor
        public class WideEnumDrawer : MaterialPropertyDrawer
        {
            private readonly GUIContent[] names;
            private readonly float[] values;
    
            // internal Unity AssemblyHelper can't be accessed
            private Type[] TypesFromAssembly(Assembly a)
            {
                if (a == null)
                    return new Type[0];
                try
                {
                    return a.GetTypes();
                }
                catch (ReflectionTypeLoadException)
                {
                    return new Type[0];
                }
            }
            public WideEnumDrawer(string enumName)
            {
                var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(
                    x => TypesFromAssembly(x)).ToArray();
                try
                {
                    var enumType = types.FirstOrDefault(
                        x => x.IsEnum && (x.Name == enumName || x.FullName == enumName)
                    );
                    var enumNames = Enum.GetNames(enumType);
                    names = new GUIContent[enumNames.Length];
                    for (int i=0; i<enumNames.Length; ++i)
                        names[i] = new GUIContent(enumNames[i]);
    
                    var enumVals = Enum.GetValues(enumType);
                    values = new float[enumVals.Length];
                    for (int i=0; i<enumVals.Length; ++i)
                        values[i] = (int)enumVals.GetValue(i);
                }
                catch (Exception)
                {
                    Debug.LogWarningFormat("Failed to create  WideEnum, enum {0} not found", enumName);
                    throw;
                }
    
            }
            
            public WideEnumDrawer(string n1, float v1) : this(new[] {n1}, new[] {v1}) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2) : this(new[] { n1, n2 }, new[] { v1, v2 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3) : this(new[] { n1, n2, n3 }, new[] { v1, v2, v3 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4) : this(new[] { n1, n2, n3, n4 }, new[] { v1, v2, v3, v4 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4, string n5, float v5) : this(new[] { n1, n2, n3, n4, n5 }, new[] { v1, v2, v3, v4, v5 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4, string n5, float v5, string n6, float v6) : this(new[] { n1, n2, n3, n4, n5, n6 }, new[] { v1, v2, v3, v4, v5, v6 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4, string n5, float v5, string n6, float v6, string n7, float v7) : this(new[] { n1, n2, n3, n4, n5, n6, n7 }, new[] { v1, v2, v3, v4, v5, v6, v7 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4, string n5, float v5, string n6, float v6, string n7, float v7, string n8, float v8) : this(new[] { n1, n2, n3, n4, n5, n6, n7, n8 }, new[] { v1, v2, v3, v4, v5, v6, v7, v8 }) {}
            public WideEnumDrawer(string n1, float v1, string n2, float v2, string n3, float v3, string n4, float v4, string n5, float v5, string n6, float v6, string n7, float v7, string n8, float v8, string n9, float v9, string n10, float v10, string n11, float v11, string n12, float v12, string n13, float v13, string n14, float v14, string n15, float v15, string n16, float v16, string n17, float v17, string n18, float v18, string n19, float v19, string n20, float v20, string n21, float v21, string n22, float v22, string n23, float v23, string n24, float v24, string n25, float v25, string n26, float v26, string n27, float v27, string n28, float v28, string n29, float v29, string n30, float v30, string n31, float v31, string n32, float v32) : this(new[] { n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32 }, new[] { v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32 }) {}
            public WideEnumDrawer(string[] enumNames, float[] vals)
            {
                names = new GUIContent[enumNames.Length];
                for (int i=0; i<enumNames.Length; ++i)
                    names[i] = new GUIContent(enumNames[i]);
    
                values = new float[vals.Length];
                for (int i=0; i<vals.Length; ++i)
                    values[i] = vals[i];
            }
    
            public override void OnGUI(Rect position, MaterialProperty prop, GUIContent label, MaterialEditor editor)
            {
                EditorGUI.showMixedValue = prop.hasMixedValue;
                EditorGUI.BeginChangeCheck();
                var value = prop.floatValue;
                int selectedIndex = -1;
                for (int i=0; i<values.Length; i++)
                    if (values[i] == value)
                    {
                        selectedIndex = i;
                        break;
                    }
    
                float labelWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 0f;
                var selIndex = EditorGUI.Popup(position, label, selectedIndex, names);
                EditorGUI.showMixedValue = false;
                if (EditorGUI.EndChangeCheck())
                    prop.floatValue = values[selIndex];
                EditorGUIUtility.labelWidth = labelWidth;
            }
    
            public override float GetPropertyHeight(MaterialProperty prop, string label, MaterialEditor editor)
            {
                return base.GetPropertyHeight(prop, label, editor);
            }
        }
        
        // IgnoreProjector, ForceNoShadowCasting | Ported from Kaj's Editor
        public class OverrideTagToggleDrawer : MaterialPropertyDrawer
        {
            readonly string tag;

            public OverrideTagToggleDrawer(string tag)
            {
                this.tag = tag;
            }

            public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor materialEditor)
            {
                EditorGUI.showMixedValue = prop.hasMixedValue;
                var flag = prop.floatValue;
                EditorGUIUtility.labelWidth = 0f;
                EditorGUI.BeginChangeCheck();
                flag = EditorGUILayout.Toggle(label, flag == 1) ? 1 : 0;
                if (EditorGUI.EndChangeCheck())
                {
                    prop.floatValue = flag;
                    if (flag == 1)
                        foreach (Material m in materialEditor.targets)
                            m.SetOverrideTag(tag, "True");
                    else 
                        foreach (Material m in materialEditor.targets)
                            m.SetOverrideTag(tag, "False");
                }
            }

            public override float GetPropertyHeight(MaterialProperty prop, string label, MaterialEditor editor)
            {
                return -2;
            }
        }
    }
}